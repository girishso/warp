defmodule Warp.ConversationService do
  alias Ecto.Multi
  alias Warp.Org.Team
  alias Warp.Org.Channel
  alias Warp.Account.User
  alias Warp.Conversation.Thread
  alias Warp.Conversation.Message
  alias Warp.Conversation
  alias Warp.Repo
  require Logger

  def create_thread_w_message(thread_attrs, message_attrs, user) do
    Logger.debug "create_thread_w_message/3 message_attrs: #{inspect message_attrs}"
    team    = Warp.Org.get_team!(thread_attrs["team_id"], user)
    channel = Conversation.get_channel!(thread_attrs["channel_id"])

    thread_changeset = user
      |> Ecto.build_assoc(:threads)
      |> Thread.changeset(thread_attrs)


    Multi.new
    |> Multi.insert(:thread, thread_changeset)
    |> Multi.run(:message, fn %{thread: thread} ->
        Conversation.create_message(message_attrs, team, channel, thread)
      end)
    |> Repo.transaction
  end

  defp set_user_team_ids(%{team: team}, user) do
    existing_team_ids = user.team_ids

    new_team_ids =
        List.insert_at(existing_team_ids, 0, team.id)
    Logger.debug "existing_team_ids: #{inspect existing_team_ids}"
    Logger.debug "new_team_ids: #{inspect new_team_ids}"
    user
    |> User.team_ids_changeset(%{team_ids: new_team_ids})
    |> Repo.update
  end
end
