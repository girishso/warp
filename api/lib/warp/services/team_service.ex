defmodule Warp.TeamService do
  alias Ecto.Multi
  alias Warp.Org.Team
  alias Warp.Account.User
  alias Warp.Org.Channel
  alias Warp.Repo
  require Logger

  def insert(team_changeset, user) do
    Multi.new
    |> Multi.insert(:team, team_changeset)
    |> Multi.run(:create_assoc_channel, &create_assoc_channel(&1))
    |> Multi.run(:set_user_team_ids, &set_user_team_ids(&1.team.id, user))
  end

  def add_user_to_team(user, team_id) do
    set_user_team_ids(team_id, user)
  end

  defp create_assoc_channel(team) do
    Channel.team_relation_changeset(team, %{name: "General"})
    |> Repo.insert
  end

  defp set_user_team_ids(team_id, user) do
    existing_team_ids = user.team_ids

    new_team_ids =
        if is_nil(existing_team_ids) do
          [team_id]
        else
          List.insert_at(existing_team_ids, 0, team_id)
        end

    Logger.debug "existing_team_ids: #{inspect existing_team_ids}"
    Logger.debug "new_team_ids: #{inspect new_team_ids}"
    user
    |> User.team_ids_changeset(%{team_ids: new_team_ids})
    |> Repo.update
  end
end
