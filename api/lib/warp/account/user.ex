defmodule Warp.Account.User do
  use Ecto.Schema
  import Ecto.Changeset


  schema "users" do
    field :email, :string
    field :name, :string
    field :password_digest, :string
    field :password, :string, virtual: true
    field :team_ids, {:array, :integer}

    has_many :teams, Warp.Org.Team
    has_many :threads, Warp.Conversation.Thread
    has_many :channels, Warp.Org.Channel

    timestamps()
  end

  @required_fields ~w(name email password)a # <- Changed from password_digest to password
  @optional_fields ~w()a

  @doc false
  def registration_changeset(struct, attrs) do
    struct
    |> cast(attrs, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
    |> unique_constraint(:name)
    |> unique_constraint(:email)
    |> put_pass_hash
  end

  def team_ids_changeset(struct, params) do
    struct
    |> cast(params, [:team_ids])
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @required_fields, @optional_fields)
    |> validate_required(@required_fields)
    |> unique_constraint(:name)
    |> unique_constraint(:email)
  end

  defp put_pass_hash(changeset) do
    password = changeset.changes.password
    put_change(changeset, :password_digest, Comeonin.Bcrypt.hashpwsalt(password))
  end
end
