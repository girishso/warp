defmodule Warp.Account do
  @moduledoc """
  The Account context.
  """

  import Ecto.Query, warn: false
  alias Warp.Repo

  alias Warp.Account.User
  require Logger

  def list_users_for_team(team) do
    query = from u in User,
              where: fragment("? = ANY(team_ids)", ^team.id)

    Repo.all(query)
  end

  def list_users_for_messages(messages) do
    flattened_user_ids =
        messages
        |> List.foldl([], fn(msg, acc) ->
                List.insert_at(acc, 0, msg.recipient_ids)
                |> List.insert_at(0, msg.sender_id)
              end)
        |> List.flatten
        |> Enum.uniq

    Logger.debug("flattened_user_ids: #{inspect flattened_user_ids}")

    # user_ids = List.insert_at(recipient_ids, 0, sender_id)

    query = from u in User,
      where: u.id in ^flattened_user_ids

    Repo.all(query)
  end

  def list_users do
    Repo.all(User)
  end

  def get_user!(id), do: Repo.get!(User, id)

  def get_by_email(email) do
    Repo.get_by(User, email: String.downcase(email))
  end

  def register_user(attrs \\ %{}) do
    %User{}
    |> User.registration_changeset(attrs)
    |> Repo.insert()
  end

  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  def delete_user(%User{} = user) do
    Repo.delete(user)
  end


  def change_user(%User{} = user) do
    User.changeset(user, %{})
  end
end
