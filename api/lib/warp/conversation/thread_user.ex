defmodule Warp.Conversation.ThreadUser do
  use Ecto.Schema
  import Ecto.Changeset
  alias Warp.Conversation.ThreadUser


  schema "threads_users" do
    field :title, :string
    field :user_id, :id
    field :thread_id, :id

    timestamps()
  end

  def touch_changeset(struct) do
    updated_at = Ecto.DateTime.utc
                  |> Ecto.DateTime.to_iso8601

    struct
    |> cast(%{ updated_at: updated_at }, [:updated_at])
  end

  @doc false
  def changeset(%ThreadUser{} = thread_user, attrs) do
    thread_user
    |> cast(attrs, [:title, :thread_id, :user_id])
    # |> validate_required([:title])
    |> foreign_key_constraint(:thread_id)
    |> foreign_key_constraint(:user_id)
    |> unique_constraint(:uniq_user_thread, name: :threads_users_unique_ix)
  end
end
