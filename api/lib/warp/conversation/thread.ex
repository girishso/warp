defmodule Warp.Conversation.Thread do
  use Ecto.Schema
  import Ecto.Changeset
  alias Warp.Conversation.Thread
  import Ecto.Query


  schema "threads" do
    field :subject, :string
    belongs_to :team, Warp.Account.Team
    belongs_to :channel, Warp.Account.Channel
    belongs_to :user, Warp.Account.User, foreign_key: :user_id
    has_many :messages, Warp.Conversation.Message
    field :un_read, :boolean, virtual: true

    timestamps()
  end

  @required_fields ~w(subject team_id channel_id user_id)a

  def touch_changeset(struct) do
    updated_at = Ecto.DateTime.utc
                  |> Ecto.DateTime.to_iso8601

    struct
    |> cast(%{ updated_at: updated_at }, [:updated_at])
  end

  @doc false
  def changeset(%Thread{} = thread, attrs) do
    thread
    |> cast(attrs, @required_fields)
    |> validate_required(@required_fields)
    |> foreign_key_constraint(:team_id)
    |> foreign_key_constraint(:user_id)
    |> assoc_constraint(:team)
    # |> update_change
    |> foreign_key_constraint(:channel_id)
    |> assoc_constraint(:channel)
  end
end
