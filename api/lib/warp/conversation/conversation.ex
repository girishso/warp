defmodule Warp.Conversation do
  import Ecto.Query, warn: false
  alias Warp.Repo
  import Ecto

  alias Warp.Conversation.Thread
  alias Warp.Conversation.Message
  alias Warp.Org.Channel
  alias Warp.Conversation.ThreadUser
  require Logger


  defp most_recent_first(query) do
    Ecto.Query.from t in query,
      order_by: [desc: t.inserted_at]
  end

  defp most_recent_last(query) do
    Ecto.Query.from t in query,
      order_by: [asc: t.inserted_at]
  end

  defp most_recently_updated_first(query) do
    Ecto.Query.from t in query,
      order_by: [desc: t.updated_at]
  end

  def list_all_messages_for_team(team, user) do
    # TODO: make sure user belongs to the Team
    Repo.all assoc(team, :messages)
    |> most_recent_last()
    # |> Repo.preload([:thread])
  end

  def list_threads_for_channel(channel) do
    Repo.all assoc(channel, :threads)
  end

  def sql_magic(result_of_Ecto_Adapters_SQL_query) do
    columns = result_of_Ecto_Adapters_SQL_query.columns |> Enum.map(&(String.to_atom(&1)))
    # Ecto.DateTime.cast!(date/time tuple)
    Enum.map result_of_Ecto_Adapters_SQL_query.rows, fn(row) ->
      Enum.zip(columns, row) |> Map.new
    end
  end

  def list_threads_for_team(team, user) do

    # {:ok, res} = Repo.query("SELECT *, (not exists (select TU.updated_at from threads_users as TU where tu.thread_id = t0.id AND tu.user_id = $1)) OR
    #             (case when
    #               (select true from threads_users as TU where tu.thread_id = t0.id AND
    #               tu.user_id = $1 AND tu.updated_at < t0.updated_at ) = true
    #               then true else false end)
    #           as un_read
    #     FROM threads AS t0 WHERE (t0.team_id = $2) ORDER BY t0.updated_at desc", [user.id, team.id])

    # sql_magic(res)
    # |> Repo.paginate

    (from threads in Thread,
      where: [team_id: ^team.id],
      order_by: [desc: :updated_at])
      |> Warp.Repo.paginate

    # Repo.all(
    #   from threads in Thread,
    #   # select: [:id, :subject, :team_id, :channel_id, :user_id, :inserted_at, :updated_at, read_by_user: true],
    #   select: %{threads | read_by_user: last_read},
    #   where: [team_id: ^team.id],
    #   order_by: [desc: :updated_at]
    # )
  end

  def list_channels_for_team(team) do
    Repo.all assoc(team, :channels)
    |> most_recent_first()
  end

  def get_thread!(id), do: Repo.get!(Thread, id)
  def get_channel!(id), do: Repo.get!(Channel, id)
  def get_message!(id), do: Repo.get!(Message, id)
  def get_team(id), do: Repo.get(Team, id)

  def create_thread(attrs \\ %{}, team, channel, user) do
    cs  = Ecto.build_assoc(team, :threads)
    cs2 = Ecto.build_assoc(channel, :threads, Map.from_struct cs)
    cs3 = Ecto.build_assoc(user, :threads, Map.from_struct cs2)
    Logger.debug "cs3: #{inspect cs3}"

    Thread.changeset(cs3, attrs)
    |> Repo.insert()
  end

  def update_thread(%Thread{} = thread, attrs) do
    thread
    |> Thread.changeset(attrs)
    |> Repo.update()
  end

  def delete_thread(%Thread{} = thread) do
    Repo.delete(thread)
  end

  def change_thread(%Thread{} = thread) do
    Thread.changeset(thread, %{})
  end

  alias Warp.Conversation.Message


  def list_messages do
    Repo.all(Message)
  end

  def get_message!(id, user), do: Repo.get_by!(Message, id: id, user_id: user.id)

  def create_message_changeset(attrs \\ %{}, user) do
    # Logger.debug "attrs: #{inspect attrs}"
    team = Warp.Org.get_team!(attrs["team_id"], user)
    thread = get_thread!(attrs["thread_id"])
    channel = get_channel!(attrs["channel_id"])


    create_message_changeset(attrs, team, channel, thread)
  end

  def create_message_changeset(attrs \\ %{}, team, channel, thread) do
    # Logger.debug "create_message_changeset/4 attrs: #{inspect attrs}"
    cs = Ecto.build_assoc(team, :messages)
    cs2 = Ecto.build_assoc(channel, :messages, Map.from_struct cs)
    cs3 = Ecto.build_assoc(thread, :messages, Map.from_struct cs2)

    # TODO: Fix - Do we really need recipient ids in message?
    attrs = Map.put_new(attrs, "recipient_ids", [1])

    Message.changeset(cs3, attrs)
  end

  def create_message(attrs \\ %{}, user) do
    create_message_changeset(attrs, user)
    |> Repo.insert()
  end

  def create_message(attrs \\ %{}, team, channel, thread) do
    create_message_changeset(attrs, team, channel, thread)
    |> Repo.insert()
  end

  def update_message(%Message{} = message, attrs) do
    message
    |> Message.changeset(attrs)
    |> Repo.update()
  end

  def delete_message(%Message{} = message) do
    Repo.delete(message)
  end

  def change_message(%Message{} = message) do
    Message.changeset(message, %{})
  end

  def upsert_user_threads(attrs \\ %{}, thread_id, user) do
      # case Repo.all(from users_threads in UserThread,
      #             where: [thread_id: ^thread_id, user_id: ^user_id],
      #             limit: 1) do
      case Repo.get_by(ThreadUser, thread_id: thread_id, user_id: user.id) do
        nil  ->
          %ThreadUser{} # not found, we build one
          |> ThreadUser.changeset(%{thread_id: thread_id, user_id: user.id})
        thread_user ->
          thread_user #  exists, let's use it
          |> ThreadUser.touch_changeset
      end

      |> Repo.insert_or_update
  end

end
