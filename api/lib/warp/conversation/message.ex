defmodule Warp.Conversation.Message do
  use Ecto.Schema
  import Ecto.Query, warn: false
  import Ecto
  import Ecto.Changeset
  alias Warp.Conversation.Message
  alias Warp.Org.Channel
  alias Warp.Conversation.Thread
  alias Warp.Repo
  require Logger

  schema "messages" do
    field :body, :string
    belongs_to :thread, Warp.Conversation.Thread
    belongs_to :team, Warp.Account.Team
    belongs_to :channel, Warp.Account.Channel
    belongs_to :user, Warp.Account.User, foreign_key: :sender_id
    field :recipient_ids, {:array, :integer}

    timestamps()
  end

  @required_fields ~w(body thread_id team_id channel_id sender_id recipient_ids)a

  @doc false
  def changeset(%Message{} = message, attrs) do
    message
    |> cast(attrs, @required_fields)
    |> validate_required(@required_fields)
    |> validate_channel_bt_team
    |> validate_thread_bt_team
    |> foreign_key_constraint(:team_id)
    |> assoc_constraint(:team)
    |> foreign_key_constraint(:channel_id)
    |> assoc_constraint(:channel)
    |> foreign_key_constraint(:thread_id)
    |> assoc_constraint(:thread)
    |> touch_thread
  end

  def touch_thread(changeset, options \\ []) do
    changeset
      |> prepare_changes(fn prepared_changeset ->
          repo = prepared_changeset.repo
          thread = Repo.get(Thread, get_field(changeset, :thread_id))

          Thread.touch_changeset(thread)
          |> repo.update!
          prepared_changeset
        end)
  end

  # TODO: a thread should not be part of two teams/channels.. add constraint
  # TODO: fix not working.. most prolly working (13th Oct 2017)
  def validate_channel_bt_team(changeset, options \\ []) do
    Logger.debug "################# changeset: #{inspect changeset}"
    # team = Repo.get(Warp.Org.Team, get_field(changeset, :team_id))

    team_id    = get_field(changeset, :team_id)
    channel_id = get_field(changeset, :channel_id)

    query = from c in Channel,
      where: c.team_id == ^team_id,
      where: c.id == ^channel_id


    case exists(query) do
      true -> changeset
      false -> add_error(changeset, :channel_id, "Channel does not belong to team #{inspect team_id}")
    end

    # validate_change(changeset, :channel_id, fn :channel_id, channel_id ->
    #   Logger.debug "@@@@@@@@@@@@@@"
    #   Logger.debug "changeset.team_id : #{inspect changeset.team_id}"
    #   team = Repo.get(Team, get_field(changeset, :team_id))
    #   case exists(Repo.all assoc(team, :channels)) do
    #     true -> []
    #     false -> [{:channel_id, options[:message] || "Channel must belong to Team"}]
    #   end
    # end)
  end

  # TODO: fix not working.. most prolly working (13th Oct 2017)
  def validate_thread_bt_team(changeset, options \\ []) do
    # team    = Repo.get(Warp.Org.Team, get_field(changeset, :team_id))
    team_id   = get_field(changeset, :team_id)
    thread_id = get_field(changeset, :thread_id)

    query = from th in Thread,
      where: th.team_id == ^team_id,
      where: th.id == ^thread_id

    case exists(query) do
      true -> changeset
      false -> add_error(changeset, :thread_id, "Thread does not belong to team #{inspect team_id}")
    end

    # validate_change(changeset, :thread_id, fn :thread_id, thread_id ->
    #   team = Repo.get(Team, get_field(changeset, :team_id))
    #   case exists(Repo.all assoc(team, :threads)) do
    #     true -> []
    #     false -> [{:thread_id, options[:message] || "Thread must belong to Team"}]
    #   end
    # end)
  end

  def exists(queryable) do
    query = Ecto.Query.from(x in queryable, select: 1, limit: 1) |> Ecto.Queryable.to_query
    # Logger.debug "**************"
    # Logger.debug "Exists : #{inspect query}"
    case Repo.all(query) do
      [1] -> true
      [] -> false
    end
  end
end
