defmodule Warp.Org.Team do
  use Ecto.Schema
  import Ecto.Changeset
  alias Warp.Org.Team


  schema "teams" do
    field :descr, :string
    field :name, :string
    belongs_to :user, Warp.Account.User
    has_many :threads, Warp.Conversation.Thread
    has_many :channels, Warp.Org.Channel
    has_many :messages, Warp.Conversation.Message

    timestamps()
  end

  @doc false
  def changeset(%Team{} = team, attrs) do
    team
    |> cast(attrs, [:name, :descr])
    |> validate_required([:name])
    |> foreign_key_constraint(:user_id)
    |> assoc_constraint(:user)
  end
end
