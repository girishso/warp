defmodule Warp.Org do
  import Ecto
  import Ecto.Query, warn: false

  alias Warp.Repo
  alias Warp.Org.Team
  alias Warp.TeamService

  def list_own_teams(user) do
    Repo.all assoc(user, :teams)
  end

  def list_all_teams_user_belongs_to(user) do
    query = from t in Team,
      where: t.id in ^user.team_ids,
      order_by: [asc: t.name]

    Repo.all(query)
  end

  def get_team!(id, user), do: Repo.get_by!(Team, id: id, user_id: user.id)

  def create_team(attrs \\ %{}, user) do
      user
      |> Ecto.build_assoc(:teams)
      |> Team.changeset(attrs)
      |> TeamService.insert(user)
      |> Repo.transaction
  end

  def update_team(%Team{} = team, attrs) do
    team
    |> Team.changeset(attrs)
    |> Repo.update()
  end

  def delete_team(%Team{} = team, user) do
    # TODO: delete team_id in User.team_ids as well
    Repo.delete(team)
  end

  def change_team(%Team{} = team, user) do
    Team.changeset(team, %{})
  end

  alias Warp.Org.Channel

  def list_channels(user) do
    query = from c in Channel,
      where: c.team_id in ^user.team_ids,
      order_by: [asc: c.name]

    Repo.all(query)
  end

  def get_channel!(id, team), do: Repo.get_by!(Channel, id: id, team_id: team.id)

  def create_channel(attrs \\ %{}, user) do
    # %Channel{}
    # |> Channel.changeset(attrs)
    # |> Repo.insert()

    changeset =
      user
      |> Ecto.build_assoc(:channels)
      |> Channel.changeset(attrs)
      |> Repo.insert()
  end

  def update_channel(%Channel{} = channel, attrs, user) do
    channel
    |> Channel.changeset(attrs)
    |> Repo.update()
  end

  def delete_channel(%Channel{} = channel, user) do
    Repo.delete(channel)
  end

  def change_channel(%Channel{} = channel, user) do
    Channel.changeset(channel, %{})
  end
end
