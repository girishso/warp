defmodule Warp.Org.Channel do
  use Ecto.Schema
  import Ecto.Changeset
  alias Warp.Org.Channel


  schema "channels" do
    field :descr, :string
    field :name, :string
    belongs_to :team, Warp.Account.Team
    belongs_to :user, Warp.Account.User
    has_many :threads, Warp.Conversation.Thread
    has_many :messages, Warp.Conversation.Message

    timestamps()
  end

  def team_relation_changeset(%{team: team}, attrs) do
    changeset(%Channel{}, Map.merge(%{team_id: team.id, user_id: team.user_id}, attrs))
  end

  @doc false
  def changeset(%Channel{} = channel, attrs) do
    channel
    |> cast(attrs, [:name, :team_id, :user_id])
    |> validate_required([:name, :team_id])
    |> foreign_key_constraint(:team_id)
    |> assoc_constraint(:team)
    # |> unique_constraint(:name)
    |> unique_constraint(:name, name: :channels_teams_unique_ix)
  end
end
