defmodule WarpWeb.SessionController do
  use WarpWeb, :controller
  alias Warp.Account
  alias Warp.Org
  
  def create(conn, params) do
    case authenticate(params) do
      {:ok, user} ->
        new_conn = Guardian.Plug.api_sign_in(conn, user, :access)
        jwt = Guardian.Plug.current_token(new_conn)
        teams = Org.list_all_teams_user_belongs_to(user)

        new_conn
        |> put_status(:created)
        |> render("show.json", user: user, jwt: jwt, teams: teams)
      :error ->
        conn
        |> put_status(:unauthorized)
        |> render("error.json")
    end
  end

  def delete(conn, _) do
    jwt = Guardian.Plug.current_token(conn)
    Guardian.revoke!(jwt)

    conn
    |> put_status(:ok)
    |> render("delete.json")
  end

  def refresh(conn, _params) do
    user = Guardian.Plug.current_resource(conn)
    jwt = Guardian.Plug.current_token(conn)
    {:ok, claims} = Guardian.Plug.claims(conn)

    case Guardian.refresh!(jwt, claims, %{ttl: {30, :days}}) do
      {:ok, new_jwt, _new_claims} ->
        conn
        |> put_status(:ok)
        |> render("show.json", user: user, jwt: new_jwt)
      {:error, _reason} ->
        conn
        |> put_status(:unauthorized)
        |> render("forbidden.json", error: "Not authenticated")
    end
  end

  def ping(conn, _) do
    conn
    |> put_status(:ok)
    |> render("ping.json")
  end

  def unauthenticated(conn, _params) do
    conn
    |> put_status(:forbidden)
    |> render(WarpWeb.SessionView, "forbidden.json", error: "Not Authenticated. unauthenticated")
  end

  def no_resource(conn, _params) do
    conn
    |> put_status(:forbidden)
    |> render(WarpWeb.SessionView, "forbidden.json", error: "Not Authenticated. no_resource")
  end

  defp authenticate(%{"email" => email, "password" => password}) do
    user = Account.get_by_email(email)

    case check_password(user, password) do
      true -> {:ok, user}
      _ -> :error
    end
  end

  defp check_password(user, password) do
    case user do
      nil -> Comeonin.Bcrypt.dummy_checkpw()
      _ -> Comeonin.Bcrypt.checkpw(password, user.password_digest)
    end
  end
end
