defmodule WarpWeb.MessageController do
  use WarpWeb, :controller
  use Guardian.Phoenix.Controller
  plug Guardian.Plug.EnsureResource, handler: WarpWeb.SessionController

  alias Warp.Conversation
  alias Warp.Conversation.Message
  alias Warp.Conversation.Thread
  alias Warp.ConversationService

  action_fallback WarpWeb.FallbackController

  def index(conn, _params, user, _) do
    messages = Conversation.list_messages(user)
    render(conn, "index.json", messages: messages)
  end

  def create(conn, %{"message" => message_params}, user, _) do
    # put_status(conn, :created)
    # send_resp(conn, :no_content, "")
    with {:ok, %Message{} = message} <- Conversation.create_message(message_params, user) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", message_path(conn, :show, message))
      |> render("show.json", message: message)
    end
  end

  def threadswm(conn, %{"message" => %{"message" => message_params, "thread" => thread_params}}, user, _) do
    # put_status(conn, :created)
    # send_resp(conn, :no_content, "")

    # we need to assign new thread id
    message_params = Map.delete(message_params, "thread_id")
    with {:ok, %{message: message, thread: thread} }
      <- ConversationService.create_thread_w_message(thread_params, message_params, user) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", message_path(conn, :show, message))
      |> render("threadswm.json", message: message, thread: thread)
    else
      {:error, _, failed_value, changes_so_far} ->
        Logger.debug("in ^ler: 2 -- #{inspect failed_value} === #{inspect changes_so_far}")
        conn
        |> put_status(:unprocessable_entity)
        |> render(WarpWeb.ChangesetView, "error.json", changeset: failed_value)
    end
  end

  def show(conn, %{"id" => id}, user, _) do
    message = Conversation.get_message!(id, user)
    render(conn, "show.json", message: message)
  end

  def update(conn, %{"id" => id, "message" => message_params}, user, _) do
    message = Conversation.get_message!(id, user)

    with {:ok, %Message{} = message} <- Conversation.update_message(message, message_params) do
      render(conn, "show.json", message: message)
    end
  end

  def delete(conn, %{"id" => id}, user, _) do
    message = Conversation.get_message!(id, user)
    with {:ok, %Message{}} <- Conversation.delete_message(message) do
      send_resp(conn, :no_content, "")
    end
  end
end
