defmodule WarpWeb.TeamController do
  use WarpWeb, :controller
  use Guardian.Phoenix.Controller
  plug Guardian.Plug.EnsureResource, handler: WarpWeb.SessionController

  alias Warp.Org
  alias Warp.Org.Team
  alias Warp.Conversation
  alias Warp.Account
  alias Warp.Repo
  require Logger

  action_fallback WarpWeb.FallbackController

  def index(conn, _params, user, _) do
    teams = Org.list_all_teams_user_belongs_to(user)
    render(conn, "index.json", teams: teams)
  end

  def inbox(conn, %{"team_id" => team_id}, user, _) do
    team = Org.get_team!(team_id, user)
    messages = Conversation.list_all_messages_for_team(team, user)
    users = Account.list_users_for_team(team)
    threads_meta = Conversation.list_threads_for_team(team, user)
    channels = Conversation.list_channels_for_team(team)

    conn = put_resp_header(conn, "cache-control", "public,max-age=10")


    render(conn, "inbox.json", messages: messages, team: team, users: users, threads: threads_meta,  channels: channels)
    # case result do

    # end
  end

  def channels(conn, %{"team_id" => team_id}, user, _) do
    team      = Org.get_team!(team_id, user)
    channels  = Conversation.list_channels_for_team(team)
    render(conn, WarpWeb.ChannelView, "index.json", channels: channels)
  end

  def create(conn, %{"team" => team_params}, user, _) do
    result = Org.create_team(team_params, user)

    case result do
      {:ok, %{team: team}} ->
        Logger.debug("in ^ler: 1")
        conn
        |> put_status(:created)
        |> put_resp_header("location", team_path(conn, :show, team))
        |> render("show.json", team: team)
      {:error, :set_team_ids, failed_value, changes_so_far} ->
        Logger.debug("in ^ler: 2 -- #{inspect failed_value} === #{inspect changes_so_far}")
        conn
        |> put_status(:unprocessable_entity)
        |> render(WarpWeb.ChangesetView, "error.json", changeset: result)
    end
  end

  def show(conn, %{"id" => id}, user, _) do
    team = Org.get_team!(id, user)
    render(conn, "show.json", team: team)
  end

  def touch_thread(conn, %{"thread_id" => thread_id}, user, _) do
    result = Conversation.upsert_user_threads(%{}, thread_id, user)

    case result do
      {:ok, _} ->
        # send_resp(conn, :ok, "")
        render(conn, "touch_thread.json")
      {:error, _} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(WarpWeb.ChangesetView, "error.json", changeset: result)
    end
  end

  def update(conn, %{"id" => id, "team" => team_params}, user, _) do
    team = Org.get_team!(id, user)

    with {:ok, %Team{} = team} <- Org.update_team(team, team_params) do
      render(conn, "show.json", team: team)
    end
  end

  def delete(conn, %{"id" => id}, user, _) do
    team = Org.get_team!(id, user)
    with {:ok, %Team{}} <- Org.delete_team(team) do
      send_resp(conn, :no_content, "")
    end
  end
end
