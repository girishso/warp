defmodule WarpWeb.ChannelController do
  use WarpWeb, :controller
  use Guardian.Phoenix.Controller
  plug Guardian.Plug.EnsureResource, handler: WarpWeb.SessionController

  alias Warp.Org
  alias Warp.Org.Channel

  action_fallback WarpWeb.FallbackController

  def index(conn, _params, user, _) do
    channels = Org.list_channels(user)
    render(conn, "index.json", channels: channels)
  end

  def create(conn, %{"channel" => channel_params}, user, _) do
    with {:ok, %Channel{} = channel} <- Org.create_channel(channel_params, user) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", channel_path(conn, :show, channel))
      |> render("show.json", channel: channel)
    end
  end

  def show(conn, %{"id" => id}, user, _) do
    channel = Org.get_channel!(id, user)
    render(conn, "show.json", channel: channel)
  end

  def update(conn, %{"id" => id, "channel" => channel_params}, user, _) do
    channel = Org.get_channel!(id, user)

    with {:ok, %Channel{} = channel} <- Org.update_channel(channel, channel_params) do
      render(conn, "show.json", channel: channel)
    end
  end

  def delete(conn, %{"id" => id}, user, _) do
    channel = Org.get_channel!(id, user)
    with {:ok, %Channel{}} <- Org.delete_channel(channel) do
      send_resp(conn, :no_content, "")
    end
  end
end
