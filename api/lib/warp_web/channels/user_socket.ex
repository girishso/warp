defmodule WarpWeb.UserSocket do
  use Phoenix.Socket
  alias Warp.Account
  import Guardian.Phoenix.Socket
  require Logger
  ## Channels
  # channel "room:*", WarpWeb.RoomChannel
  channel "inbox", WarpWeb.InboxChannel

  ## Transports
  transport :websocket, Phoenix.Transports.WebSocket, timeout: 45_000
  # transport :longpoll, Phoenix.Transports.LongPoll

  # Socket params are passed from the client and can
  # be used to verify and authenticate a user. After
  # verification, you can put default assigns into
  # the socket that will be set for all channels, ie
  #
  #     {:ok, assign(socket, :user_id, verified_user_id)}
  #
  # To deny connection, return `:error`.
  #
  # See `Phoenix.Token` documentation for examples in
  # performing token verification on connect.

  def connect(%{"token" => jwt} = params, socket) do
    case sign_in(socket, jwt) do
      {:ok, authed_socket, guardian_params} ->
        Logger.debug "  > socket connect success"
        {:ok, authed_socket}
       x ->
        Logger.debug "  > socket connect FAILED: #{inspect x}"
        #unauthenticated socket
        :error
    end
  end

  # def connect(_params, _socket) do
  #   Logger.debug "  > socket connect FAILED"
  #   :error
  # end

  # def id(socket), do: "users_socket:#{Guardian.Phoenix.Socket.current_resource(socket)}"

  # def connect(%{"token" => token}, socket) do
  #   # 1 day = 86400 seconds
  #   case Guardian.Phoenix.Socket.authenticate(socket, Guardian, token) do
  #     {:ok, authed_socket} ->
  #       Logger.debug "  > socket connect success"
  #       {:ok, authed_socket}
  #     {:error, x} ->
  #       Logger.debug "  > socket connect FAILED: #{inspect x}"
  #       :error
  #   end
  # end

  # def connect(_params, socket) do
  #   {:ok, socket}
  # end

  # Socket id's are topics that allow you to identify all sockets for a given user:
  #
  #     def id(socket), do: "user_socket:#{socket.assigns.user_id}"
  #
  # Would allow you to broadcast a "disconnect" event and terminate
  # all active sockets and channels for a given user:
  #
  #     WarpWeb.Endpoint.broadcast("user_socket:#{user.id}", "disconnect", %{})
  #
  # Returning `nil` makes this socket anonymous.
  def id(_socket), do: nil
end
