defmodule WarpWeb.InboxChannel do
  use Phoenix.Channel
  # alias WarpWeb.{Team, Repo}
  alias Warp.Org
  alias Warp.Org.Team
  import Ecto.Query, only: [order_by: 2]
  alias Warp.Account

  require Logger

  def join("inbox", _, socket), do: {:ok, socket}

  def handle_in("inbox:fetch_teams", params, socket) do
    Logger.info "Handling teams..."

    # search = Map.get(params, "search") || ""
    user = Warp.Account.get_user!(289)
    teams = Org.list_all_teams_user_belongs_to(user)
    ta = Enum.map(teams, fn (t) -> to_storeable_map(t) end)

    # {:reply, {:ok, teams}, socket}
    {:reply, {:ok, %{teams: ta}}, socket}
  end

  @schema_meta_fields [:__meta__]

  def to_storeable_map(struct) do
    association_fields = struct.__struct__.__schema__(:associations)
    waste_fields = association_fields ++ @schema_meta_fields

    struct |> Map.from_struct |> Map.drop(waste_fields)
  end
  # def handle_in("inbox:" <> team_id, _, socket) do
  #   Logger.info "Handling team..."

  #   team = Team
  #   |> Repo.get(team_id)

  #   case team do
  #     nil ->
  #       {:reply, {:error, %{error: "Team no found"}}, socket}
  #     _ ->
  #       {:reply, {:ok, team}, socket}
  #   end
  # end
end
