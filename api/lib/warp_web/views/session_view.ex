defmodule WarpWeb.SessionView do
  use WarpWeb, :view

  def render("show.json", %{user: user, jwt: jwt, teams: teams}) do
    %{
      data: render_one(user, WarpWeb.UserView, "user.json"),
      meta: %{token: jwt},
      teams: render_many(teams, WarpWeb.TeamView, "team.json")
    }
  end

  def render("error.json", _) do
    %{error: "Invalid email or password"}
  end

  def render("delete.json", _) do
    %{ok: true}
  end

  def render("ping.json", _) do
    %{ok: true}
  end

  def render("forbidden.json", %{error: error}) do
    %{error: error}
  end
end
