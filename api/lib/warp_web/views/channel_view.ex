defmodule WarpWeb.ChannelView do
  use WarpWeb, :view
  alias WarpWeb.ChannelView

  def render("index.json", %{channels: channels}) do
    %{channels: render_many(channels, ChannelView, "channel.json")}
  end

  def render("show.json", %{channel: channel}) do
    render_one(channel, ChannelView, "channel.json")
  end

  def render("channel.json", %{channel: channel}) do
    %{id: channel.id,
      name: channel.name,
      descr: channel.descr}
  end
end
