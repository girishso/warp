defmodule WarpWeb.TeamView do
  use WarpWeb, :view
  alias WarpWeb.TeamView
  alias WarpWeb.UserView
  alias WarpWeb.ChannelView
  alias WarpWeb.MessageView

  def render("index.json", %{teams: teams}) do
    %{teams: render_many(teams, TeamView, "team.json")}
  end

  def render("inbox.json", %{messages: messages, team: team, users: users, threads: threads, channels: channels}) do
    %{messages: render_many(messages, MessageView, "message.json"),
      team: render_one(team, TeamView, "team.json"),
      users: render_many(users, UserView, "user.json"),
      threads: render_many(threads, TeamView, "thread.json"),
      channels: render_many(channels, ChannelView, "channel.json")}
  end

  def render("show.json", %{team: team}) do
    render_one(team, TeamView, "team.json")
  end

  def render("team.json", %{team: team}) do
    %{id: team.id,
      name: team.name,
      descr: team.descr}
  end

  def render("thread.json", %{team: thread}) do
    un_read = if (thread.un_read == nil), do: false, else: thread.un_read

    %{id: thread.id,
      subject: thread.subject,
      channel_id: thread.channel_id,
      team_id: thread.team_id,
      user_id: thread.user_id,
      un_read: un_read,
      inserted_at: Ecto.DateTime.cast!(thread.inserted_at)
      }
  end

  def render("touch_thread.json", _) do
    %{ok: true}
  end
end
