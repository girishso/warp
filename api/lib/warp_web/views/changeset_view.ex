defmodule WarpWeb.ChangesetView do
  use WarpWeb, :view

  @doc """
  Traverses and translates changeset errors.

  See `Ecto.Changeset.traverse_errors/2` and
  `WarpWeb.ErrorHelpers.translate_error/1` for more details.
  """
  def translate_errors(changeset) do
    Ecto.Changeset.traverse_errors(changeset, &translate_error/1)
  end

  def render("error.json", %{changeset: changeset}) do
    # When encoded, the changeset returns its errors
    # as a JSON object. So we just pass it forward.

    # join errors to string, to simplify fe
    errors = translate_errors(changeset)
              |> Enum.map(fn({k, v}) -> "#{k}: #{v}" end)
              |> Enum.join(", ")

    %{error: errors }
  end
end
