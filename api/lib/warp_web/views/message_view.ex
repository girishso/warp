defmodule WarpWeb.MessageView do
  use WarpWeb, :view
  alias WarpWeb.MessageView
  alias WarpWeb.TeamView

  def render("index.json", %{messages: messages}) do
    %{messages: render_many(messages, MessageView, "message.json")}
  end

  def render("show.json", %{message: message}) do
    render_one(message, MessageView, "message.json")
  end

  def render("threadswm.json", %{message: message, thread: thread}) do
    %{
      message: render_one(message, MessageView, "message.json"),
      thread: render_one(thread, TeamView, "thread.json")
    }
  end

  def render("message.json", %{message: message}) do
    {:ok, inserted_ago} =  message.inserted_at |> Timex.format("{relative}", :relative)

    %{id: message.id,
      body: message.body,
      channel_id: message.channel_id,
      recipient_ids: message.recipient_ids,
      team_id: message.team_id,
      thread_id: message.thread_id,
      sender_id: message.sender_id,
      inserted_at: message.inserted_at,
      inserted_ago: inserted_ago}
  end

end
