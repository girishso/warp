defmodule WarpWeb.Router do
  use WarpWeb, :router

  pipeline :api do
    # plug CORSPlug, origin: ["*"]
    plug :accepts, ["json"]
    plug Guardian.Plug.VerifyHeader, realm: "Bearer"
    plug Guardian.Plug.LoadResource

  end

  scope "/api", WarpWeb do
    pipe_through :api

    resources "/users", UserController, except: [:new, :edit]
    options "/sessions", SessionController, :options
    post "/sessions", SessionController, :create
    delete "/sessions", SessionController, :delete
    post "/sessions/refresh", SessionController, :refresh
    get "/sessions/ping", SessionController, :ping

    resources "/teams", TeamController, except: [:new, :edit] do
      get "/inbox", TeamController, :inbox, as: :inbox
      get "/channels", TeamController, :channels, as: :channels
    end

    post "/teams/touch_thread", TeamController, :touch_thread, as: :touch_thread

    resources "/channels", ChannelController, except: [:new, :edit]


    resources "/messages", MessageController, except: [:new, :edit] do

    end
    post "/messages/threadswm", MessageController, :threadswm, as: :threadswm

  end
end
