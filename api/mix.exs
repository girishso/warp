defmodule Warp.Mixfile do
  use Mix.Project

  def project do
    [
      app: :warp,
      version: "0.0.1",
      elixir: "~> 1.4",
      elixirc_paths: elixirc_paths(Mix.env),
      compilers: [:phoenix, :gettext] ++ Mix.compilers,
      start_permanent: Mix.env == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Warp.Application, []},
      extra_applications: applications(Mix.env)
    ]
  end

  defp applications(:dev), do: applications(:all) ++ [:remix, :faker]
  defp applications(_all), do: [:logger, :runtime_tools, :comeonin, :scrivener_ecto]

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_),     do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.3.0"},
      {:phoenix_pubsub, "~> 1.0"},
      {:phoenix_ecto, "~> 3.3.0"},
      {:postgrex, ">= 0.13.5"},
      {:gettext, "~> 0.11"},
      {:comeonin, "~> 3.0"},
      {:guardian, "~> 0.14.0"},
      {:cors_plug, "~> 1.5.2"},
      {:cowboy, "~> 1.0"},
      {:remix, "~> 0.0.1", only: :dev},
      {:faker, "~> 0.8",  only: [:dev, :test]},
      {:scrivener_ecto, "~> 1.0"},
      {:distillery, "~> 1.5", runtime: false},
      {:edeliver, "~> 1.4.5"},
      {:timex, "~> 3.0"}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      "test": ["ecto.create --quiet", "ecto.migrate", "test"]
    ]
  end
end
