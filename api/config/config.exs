# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :warp,
  ecto_repos: [Warp.Repo]

# Configures the endpoint
config :warp, WarpWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "F0LcxL+eaOz5a78LyF+hKhGVNWm4ZfK2HX78OD8i6TjatfjlnW6vRZGBy+cZxAqG",
  render_errors: [view: WarpWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: Warp.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
