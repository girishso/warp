use Mix.Config

config :warp, WarpWeb.Endpoint,
  load_from_system_env: true,
  http: [port: "${PORT}"],
  check_origin: false,
  server: true,
  root: ".",
  url: [scheme: "https", host: "serene-shore-69882.herokuapp.com", port: 443],
  force_ssl: [rewrite_on: [:x_forwarded_proto]],
  secret_key_base: System.get_env("SECRET_KEY_BASE")
  # cache_static_manifest: "priv/static/cache_manifest.json"

# Do not print debug messages in production
config :logger, level: :info

# config :guardian, Guardian,
#   secret_key: System.get_env("GUARDIAN_SECRET_KEY")

config :guardian, Guardian,
  secret_key: "hKhGVNWm4ZfK2HX78OD8i6TjatfjlnW6vRZGByhKhGVNWm4ZfK2HX78OD8i6TjatfjlnW6vRZGBy",
  serializer: Warp.GuardianSerializer

config :warp, Warp.Repo,
  adapter: Ecto.Adapters.Postgres,
  url: System.get_env("DATABASE_URL"),
  pool_size: String.to_integer(System.get_env("POOL_SIZE") || "10"),
  ssl: true

# import_config "prod.secret.exs"
