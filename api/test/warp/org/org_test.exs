defmodule Warp.OrgTest do
  use Warp.DataCase

  alias Warp.Org

  describe "teams" do
    alias Warp.Org.Team

    @valid_attrs %{descr: "some descr", name: "some name"}
    @update_attrs %{descr: "some updated descr", name: "some updated name"}
    @invalid_attrs %{descr: nil, name: nil}

    def team_fixture(attrs \\ %{}) do
      {:ok, team} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Org.create_team()

      team
    end

    test "list_teams/0 returns all teams" do
      team = team_fixture()
      assert Org.list_teams() == [team]
    end

    test "get_team!/1 returns the team with given id" do
      team = team_fixture()
      assert Org.get_team!(team.id) == team
    end

    test "create_team/1 with valid data creates a team" do
      assert {:ok, %Team{} = team} = Org.create_team(@valid_attrs)
      assert team.descr == "some descr"
      assert team.name == "some name"
    end

    test "create_team/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Org.create_team(@invalid_attrs)
    end

    test "update_team/2 with valid data updates the team" do
      team = team_fixture()
      assert {:ok, team} = Org.update_team(team, @update_attrs)
      assert %Team{} = team
      assert team.descr == "some updated descr"
      assert team.name == "some updated name"
    end

    test "update_team/2 with invalid data returns error changeset" do
      team = team_fixture()
      assert {:error, %Ecto.Changeset{}} = Org.update_team(team, @invalid_attrs)
      assert team == Org.get_team!(team.id)
    end

    test "delete_team/1 deletes the team" do
      team = team_fixture()
      assert {:ok, %Team{}} = Org.delete_team(team)
      assert_raise Ecto.NoResultsError, fn -> Org.get_team!(team.id) end
    end

    test "change_team/1 returns a team changeset" do
      team = team_fixture()
      assert %Ecto.Changeset{} = Org.change_team(team)
    end
  end

  describe "channels" do
    alias Warp.Org.Channel

    @valid_attrs %{descr: "some descr", name: "some name"}
    @update_attrs %{descr: "some updated descr", name: "some updated name"}
    @invalid_attrs %{descr: nil, name: nil}

    def channel_fixture(attrs \\ %{}) do
      {:ok, channel} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Org.create_channel()

      channel
    end

    test "list_channels/0 returns all channels" do
      channel = channel_fixture()
      assert Org.list_channels() == [channel]
    end

    test "get_channel!/1 returns the channel with given id" do
      channel = channel_fixture()
      assert Org.get_channel!(channel.id) == channel
    end

    test "create_channel/1 with valid data creates a channel" do
      assert {:ok, %Channel{} = channel} = Org.create_channel(@valid_attrs)
      assert channel.descr == "some descr"
      assert channel.name == "some name"
    end

    test "create_channel/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Org.create_channel(@invalid_attrs)
    end

    test "update_channel/2 with valid data updates the channel" do
      channel = channel_fixture()
      assert {:ok, channel} = Org.update_channel(channel, @update_attrs)
      assert %Channel{} = channel
      assert channel.descr == "some updated descr"
      assert channel.name == "some updated name"
    end

    test "update_channel/2 with invalid data returns error changeset" do
      channel = channel_fixture()
      assert {:error, %Ecto.Changeset{}} = Org.update_channel(channel, @invalid_attrs)
      assert channel == Org.get_channel!(channel.id)
    end

    test "delete_channel/1 deletes the channel" do
      channel = channel_fixture()
      assert {:ok, %Channel{}} = Org.delete_channel(channel)
      assert_raise Ecto.NoResultsError, fn -> Org.get_channel!(channel.id) end
    end

    test "change_channel/1 returns a channel changeset" do
      channel = channel_fixture()
      assert %Ecto.Changeset{} = Org.change_channel(channel)
    end
  end
end
