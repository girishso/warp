defmodule Warp.TeamTest do
  use Warp.ModelCase

  alias Warp.Team

  @valid_attrs %{descr: "some descr", name: "some name"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Team.changeset(%Team{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Team.changeset(%Team{}, @invalid_attrs)
    refute changeset.valid?
  end
end
