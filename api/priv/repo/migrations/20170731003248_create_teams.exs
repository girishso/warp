defmodule Warp.Repo.Migrations.CreateTeams do
  use Ecto.Migration

  def change do
    create table(:teams) do
      add :name, :string
      add :descr, :text
      add :user_id, references(:users, on_delete: :delete_all)

      timestamps()
    end

    create index(:teams, [:user_id])
  end
end
