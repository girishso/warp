defmodule Warp.Repo.Migrations.CreateUsersThreads do
  use Ecto.Migration

  def change do
    create table(:users_threads) do
      add :thread_id, references(:threads)
      add :user_id, references(:users)

      timestamps()  # inserted_at and updated_at
    end
    create unique_index(:users_threads, [:thread_id, :user_id], name: :users_threads_unique_ix)
  end
end
