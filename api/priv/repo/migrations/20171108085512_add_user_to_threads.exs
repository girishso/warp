defmodule Warp.Repo.Migrations.AddUserToThreads do
  use Ecto.Migration

  def change do
    alter table(:threads) do
      add :user_id, references(:users, on_delete: :delete_all)
    end
  end
end
