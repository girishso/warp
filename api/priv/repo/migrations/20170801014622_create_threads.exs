defmodule Warp.Repo.Migrations.CreateThreads do
  use Ecto.Migration

  def change do
    create table(:threads) do
      add :subject, :string
      add :channel_id, references(:channels, on_delete: :delete_all)
      add :team_id, references(:teams, on_delete: :delete_all)

      timestamps()
    end

    create index(:threads, [:channel_id])
    create index(:threads, [:team_id])
  end
end
