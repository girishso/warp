defmodule Warp.Repo.Migrations.CreateChannels do
  use Ecto.Migration

  def change do
    create table(:channels) do
      add :name, :string
      add :descr, :text
      add :team_id, references(:teams, on_delete: :delete_all)

      timestamps()
    end

    create index(:channels, [:team_id])
    create unique_index(:channels, [:name, :team_id], name: :channels_teams_unique_ix)
  end
end
