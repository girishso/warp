defmodule Warp.Repo.Migrations.CreateMessages do
  use Ecto.Migration

  def change do
    create table(:messages) do
      add :body, :text
      add :thread_id, references(:threads, on_delete: :delete_all)
      add :channel_id, references(:channels, on_delete: :delete_all)
      add :team_id, references(:teams, on_delete: :delete_all)
      add :recipient_ids, {:array, :integer}, default: []
      # add :sender_id, references(:users, on_delete: :nothing)
      add :sender_id, :integer

      timestamps()
    end

    create index(:messages, [:thread_id])
    create index(:messages, [:channel_id])
    create index(:messages, [:team_id])
    create index(:messages, [:recipient_ids])
  end
end
