defmodule Warp.Repo.Migrations.CreateThreadsUsers do
  use Ecto.Migration

  def change do
    create table(:threads_users) do
      add :title, :string
      add :user_id, references(:users, on_delete: :delete_all)
      add :thread_id, references(:threads, on_delete: :delete_all)

      timestamps()
    end

    create unique_index(:threads_users, [:thread_id, :user_id], name: :threads_users_unique_ix)
  end
end
