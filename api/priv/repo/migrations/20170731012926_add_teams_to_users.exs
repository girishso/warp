defmodule Warp.Repo.Migrations.AddTeamsToUsers do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :team_ids, {:array, :integer}, default: []
    end
  end
end
