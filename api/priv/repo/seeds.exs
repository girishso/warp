# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Warp.Repo.insert!(%Warp.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Warp.Account
alias Warp.Account.User
alias Warp.Repo
alias Faker
alias Warp.Org
alias Warp.Conversation
alias Warp.TeamService
require Logger



Repo.delete_all(User)

{:ok, jon_snow} = Account.register_user(
  %{email: "jonsnow@example.com", name: "Jon Snow", password: "secret"}
  )

{:ok, arya} = Account.register_user(
  %{email: "aryastark@example.com", name: "Arya Stark", password: "secret"}
  )

{:ok, tyrion} = Account.register_user(
  %{email: "tyrion@lannister.com", name: "Tyrion Lannister", password: "secret"}
  )

{:ok, denaris} = Account.register_user(
  %{email: "denaris@targarian.com", name: "Denaris Targarian", password: "secret"}
  )

js = Account.get_user!(jon_snow.id)
{:ok, %{team: t1}} = Org.create_team(%{name: Faker.Team.name, descr: Faker.Lorem.sentence(3, ". ")}, js)

js = Account.get_user!(jon_snow.id)
{:ok, %{team: t2}} = Org.create_team(%{name: Faker.Team.name, descr: Faker.Lorem.sentence(3, ". ")}, js)

Logger.debug "arya team_ids: #{inspect arya.team_ids}"
TeamService.add_user_to_team(arya, t1.id)
arya = Account.get_user!(arya.id)
TeamService.add_user_to_team(arya, t2.id)

Logger.debug "tyrion team_ids: #{inspect tyrion.team_ids}"
TeamService.add_user_to_team(tyrion, t1.id)
tyrion = Account.get_user!(tyrion.id)
TeamService.add_user_to_team(tyrion, t2.id)

Logger.debug "denaris team_ids: #{inspect denaris.team_ids}"
TeamService.add_user_to_team(denaris, t1.id)
denaris = Account.get_user!(denaris.id)
TeamService.add_user_to_team(denaris, t2.id)

t = Org.get_team!(t1.id, js)
{:ok, c1} = Org.create_channel(%{name: Faker.Team.name, descr: Faker.Lorem.sentence(3, ". ")}, t)
{:ok, c2} = Org.create_channel(%{name: Faker.Team.name, descr: Faker.Lorem.sentence(3, ". ")}, t)

t = Org.get_team!(t2.id, js)
{:ok, c3} = Org.create_channel(%{name: Faker.Team.name, descr: Faker.Lorem.sentence(3, ". ")}, t)

c = Org.get_channel!(c1.id, t1)
{:ok, th1} = Conversation.create_thread(%{subject: Faker.Lorem.sentence(%Range{first: 4, last: 10})}, t1, c, js)
{:ok, msg1} = Conversation.create_message(%{body: Faker.Lorem.Shakespeare.hamlet, sender_id: js.id, recipient_ids: [arya.id, tyrion.id]},      t1, c, th1)
{:ok, msg1} = Conversation.create_message(%{body: Faker.Lorem.Shakespeare.hamlet, sender_id: arya.id, recipient_ids: [denaris.id, tyrion.id]}, t1, c, th1)
{:ok, msg1} = Conversation.create_message(%{body: Faker.Lorem.Shakespeare.hamlet, sender_id: js.id, recipient_ids: [arya.id, tyrion.id]},      t1, c, th1)
{:ok, msg1} = Conversation.create_message(%{body: Faker.Lorem.Shakespeare.hamlet, sender_id: arya.id, recipient_ids: [denaris.id, tyrion.id]}, t1, c, th1)
{:ok, msg1} = Conversation.create_message(%{body: Faker.Lorem.Shakespeare.hamlet, sender_id: arya.id, recipient_ids: [denaris.id, tyrion.id]}, t1, c, th1)

{:ok, th2} = Conversation.create_thread(%{subject: Faker.Lorem.sentence(%Range{first: 4, last: 10})}, t1, c, js)
{:ok, msg1} = Conversation.create_message(%{body: Faker.Lorem.Shakespeare.hamlet, sender_id: js.id, recipient_ids: [arya.id, tyrion.id]},      t1, c, th2)
{:ok, msg1} = Conversation.create_message(%{body: Faker.Lorem.Shakespeare.hamlet, sender_id: denaris.id, recipient_ids: [arya.id, tyrion.id]}, t1, c, th2)
{:ok, msg1} = Conversation.create_message(%{body: Faker.Lorem.Shakespeare.hamlet, sender_id: denaris.id, recipient_ids: [arya.id, tyrion.id]}, t1, c, th2)
{:ok, msg1} = Conversation.create_message(%{body: Faker.Lorem.Shakespeare.hamlet, sender_id: denaris.id, recipient_ids: [arya.id, tyrion.id]}, t1, c, th2)
{:ok, msg1} = Conversation.create_message(%{body: Faker.Lorem.Shakespeare.hamlet, sender_id: denaris.id, recipient_ids: [arya.id, tyrion.id]}, t1, c, th2)


{:ok, th2} = Conversation.create_thread(%{subject: Faker.Lorem.sentence(%Range{first: 4, last: 10})}, t1, c2, js)
{:ok, msg1} = Conversation.create_message(%{body: Faker.Lorem.Shakespeare.hamlet, sender_id: js.id, recipient_ids: [arya.id, tyrion.id]},      t1, c, th2)
{:ok, msg1} = Conversation.create_message(%{body: Faker.Lorem.Shakespeare.hamlet, sender_id: denaris.id, recipient_ids: [arya.id, tyrion.id]}, t1, c, th2)
{:ok, msg1} = Conversation.create_message(%{body: Faker.Lorem.Shakespeare.hamlet, sender_id: denaris.id, recipient_ids: [arya.id, tyrion.id]}, t1, c, th2)
{:ok, msg1} = Conversation.create_message(%{body: Faker.Lorem.Shakespeare.hamlet, sender_id: denaris.id, recipient_ids: [arya.id, tyrion.id]}, t1, c, th2)

{:ok, th2} = Conversation.create_thread(%{subject: Faker.Lorem.sentence(%Range{first: 4, last: 10})}, t1, c2, js)

{:ok, th3} = Conversation.create_thread(%{subject: Faker.Lorem.sentence(%Range{first: 3, last: 10})}, t2, c3, js)
{:ok, msg1} = Conversation.create_message(%{body: Faker.Lorem.Shakespeare.hamlet, sender_id: js.id, recipient_ids: [arya.id, tyrion.id]}, t2, c3, th3)
{:ok, msg1} = Conversation.create_message(%{body: Faker.Lorem.Shakespeare.hamlet, sender_id: denaris.id, recipient_ids: [arya.id, tyrion.id]}, t2, c3, th3)
{:ok, msg1} = Conversation.create_message(%{body: Faker.Lorem.Shakespeare.hamlet, sender_id: denaris.id, recipient_ids: [arya.id, tyrion.id]}, t2, c3, th3)
{:ok, msg1} = Conversation.create_message(%{body: Faker.Lorem.Shakespeare.hamlet, sender_id: denaris.id, recipient_ids: [arya.id, tyrion.id]}, t2, c3, th3)
