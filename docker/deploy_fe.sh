#!/bin/bash

echo "Did you update the prod url?"
echo "**"

DEST=~/work/misc/elixir/warp/frontend

cd $DEST

ELM_DEBUGGER=true elm-app build

echo "body > div:nth-child(2) { display: none; }" >> ./build/inbox.css

surge -p ./build --domain warp.surge.sh

# cd "$DEST/build"

# mv index.html home.html
# echo "<?php include_once('home.html'); ?>" > index.php

# #create new git repository and add everything
# git init
# git add .
# git commit -m"init"
# git remote add heroku https://git.heroku.com/warp37.git

# #push back to heroku, open web browser, and remove git repository
# git push heroku master --force
# heroku open
# # rm -fr $DEST

#go back to wherever we started.
cd -
