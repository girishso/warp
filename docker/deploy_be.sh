#!/bin/bash

DEST=/tmp/warp_api

rm -rf $DEST

cp -R ./api $DEST

cd $DEST

#create new git repository and add everything
git init
git add .
git commit -m"init"
git remote add heroku https://git.heroku.com/serene-shore-69882.git

#push back to heroku, open web browser, and remove git repository
git push heroku master --force
heroku open
# rm -fr $DEST

#go back to wherever we started.
cd -
