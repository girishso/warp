import "./main.css";
import logoPath from "./logo.svg";
import { Elm } from "./App.elm";

var app = Elm.App.init({
  node: document.getElementById('root'),
  flags: {
    showSwitchTeamsTooltip: JSON.parse(
      localStorage.getItem("showSwitchTeamsTooltip") || true
    ),
    token: JSON.parse(localStorage.getItem("token")) || null,
    env: process.env.ELM_APP_ENV
  }
});

console.log("portz", app)

app.ports.setToken.subscribe(state => {
  localStorage.setItem("token", state);
});


app.ports.setHideSwitchTeamsTT.subscribe(() => {
  localStorage.setItem("showSwitchTeamsTooltip", false);
});

app.ports.scrollTo.subscribe(elementId => {
  window.setTimeout(() => {
    var wrapper = document.getElementById("msg-inv-wrapper");
    var objDiv = document.getElementById(elementId);

    if (objDiv !== null && wrapper !== null) {
      wrapper.style.transition = "opacity 3s ease";
      wrapper.style.opacity = 0;

      window.setTimeout(() => {
        objDiv.scrollTop = objDiv.scrollHeight;
        window.setTimeout(() => {
          wrapper.style.opacity = 1;
        }, 100);
      }, 100);
    } else {
      console.log("wrapper not found");
    }
  }, 100);
});

const ping = () => {
  const xmlhttp = new XMLHttpRequest();

  xmlhttp.onreadystatechange = () => {
    // if (xmlhttp.readyState == XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
    //    if (xmlhttp.status == 200) {
    //      alert(xmlhttp.responseText)
    //    }
    //    else if (xmlhttp.status == 400) {
    //       alert('There was an error 400')
    //    }
    //    else {
    //        alert('something else other than 200 was returned ')
    //    }
    // }
  };

  xmlhttp.open(
    "GET",
    "https://serene-shore-69882.herokuapp.com/api/sessions/ping",
    true
  );
  xmlhttp.send();
};

ping();
