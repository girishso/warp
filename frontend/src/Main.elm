module Main exposing (Msg(..), gotoRoute, init, layout, layoutLogin, navBar, toast, update, updatePage, view, viewPage)

-- import Phoenix.Socket exposing (..)

import Api
import Api.Profile as Profile
import Api.Signup as Signup
import Api.User as User
import Browser
import Browser.Navigation as Navigation
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Json.Decode as JD
import Json.Encode as JE
import Model exposing (..)
import Page.Home as HomePage
import Page.Inbox.CreateChannel as CreateChannelPage
import Page.Inbox.Onboarding as OnboardingPage
import Page.Inbox.Team as TeamPage
import Page.NotFound
import Page.User.Login as LoginPage
import Page.User.Profile as ProfilePage
import Page.User.Signup as SignupPage
import Ports exposing (..)
import Process
import RemoteData exposing (..)
import Routing exposing (..)
import Task
import Time
import Types exposing (..)
import Url exposing (Url)
import Utils exposing (..)


type Msg
    = TeamMsg TeamPage.Msg
    | LoginMsg LoginPage.Msg
    | SignupMsg SignupPage.Msg
    | HomeMsg HomePage.Msg
    | OnboardingMsg OnboardingPage.Msg
    | CreateChannelMsg CreateChannelPage.Msg
    | ProfileMsg ProfilePage.Msg
    | HideToast
    | LogoutClicked
      -- | PhoenixMsg (Phoenix.Socket.Msg Msg)
      -- | FetchTeams
      -- | JoinInboxChannel
      -- | ReceiveTeams JE.Value
      -- | ShowJoinedMessage String
      -- | ShowLeftMessage String
    | HideSwitchTeamsTT
    | NoOp
    | LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url



{-
   Todo: Good to have initialModel
-}


init : JD.Value -> Url -> Navigation.Key -> ( Model, Cmd Msg )
init jdv url key =
    let
        flags =
            case JD.decodeValue flagsDecoder jdv of
                Err _ ->
                    Debug.todo "gracefully handle complete failure"

                Ok flagsx ->
                    flagsx

        currentRoute =
            parseUrlFragmentAsPath url

        ctx =
            initContext key url flags.token flags.showSwitchTeamsTooltip flags.env

        _ =
            Debug.log "init url" url

        -- { token = flags.token
        -- , showSwitchTeamsTooltip = flags.showSwitchTeamsTooltip
        -- , url = url
        -- , pageState =  NotFoundPage
        -- , route = NotFoundRoute
        -- , errorMsg = Nothing
        -- }
        -- { ctx | token = flags.token, showSwitchTeamsTooltip = flags.showSwitchTeamsTooltip, url = url }
    in
    case ctx.token of
        Nothing ->
            if routeRequiresAuth currentRoute then
                ( { pageState = LoginPage User.initialModel
                  , route = LoginRoute
                  , ctx = { ctx | errorMsg = Just "Login required" }
                  }
                , Navigation.pushUrl ctx.key "/#/login"
                )

            else
                gotoRoute currentRoute
                    { pageState = NotFoundPage
                    , route = currentRoute
                    , ctx = ctx
                    }

        Just token ->
            gotoRoute currentRoute
                { pageState = NotFoundPage
                , route = currentRoute
                , ctx = ctx
                }


flagsDecoder : JD.Decoder Flags
flagsDecoder =
    JD.map3 Flags
        (JD.field "showSwitchTeamsTooltip" JD.bool)
        (JD.field "token" (JD.nullable User.decodeToken))
        (JD.field "env" User.envDecoder)


fetchDataForRoute : Model -> ( Model, Cmd Msg )
fetchDataForRoute model =
    case model.route of
        TeamRoute id ->
            TeamPage.initAndGetTeams model.ctx
                |> Tuple.mapBoth (\subModel -> { model | pageState = TeamPage subModel }) (Cmd.map TeamMsg)

        LoginRoute ->
            ( model, Cmd.none )

        SignupRoute ->
            ( model, Cmd.none )

        HomeRoute ->
            ( model, Cmd.none )

        NotFoundRoute ->
            ( model, Cmd.none )

        ProfileRoute ->
            ( model, Cmd.none )

        OnboardingRoute ->
            ( model, Cmd.none )

        CreateChannelRoute ->
            ( model, Cmd.none )



-- update
--
--initEmptyPhxSocket : Phoenix.Socket.Socket Msg
--initEmptyPhxSocket =
--    Phoenix.Socket.init ""
--        |> Phoenix.Socket.withDebug
--        |> Phoenix.Socket.withoutHeartbeat
--
--
--initPhxSocket : Context -> Phoenix.Socket.Socket Msg
--initPhxSocket ctx =
--    let
--        wsUrl =
--            case ctx.token of
--                Just token ->
--                    Utils.webSocket_Url ++ token.meta.token
--
--                Nothing ->
--                    ""
--    in
--    Phoenix.Socket.init wsUrl
--        |> Phoenix.Socket.withDebug
--        |> Phoenix.Socket.withoutHeartbeat
--        |> Phoenix.Socket.on "inbox:fetch_teams" "inbox" ReceiveTeams


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        LinkClicked urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    case url.fragment of
                        Nothing ->
                            ( model, Cmd.none )

                        Just "" ->
                            ( model, Cmd.none )

                        Just _ ->
                            ( model, Navigation.pushUrl model.ctx.key (url |> Url.toString) )

                Browser.External href ->
                    ( model, Cmd.none )

        UrlChanged url ->
            gotoRoute (parseUrlFragmentAsPath url) model

        _ ->
            let
                ( newModel, newMsg, newCtx ) =
                    updatePage model.ctx model.pageState msg model

                modelWithNewContext =
                    { newModel | ctx = newCtx }
            in
            ( modelWithNewContext, newMsg )



{-
   PhoenixMsg msg ->
       let
           ( phxSocket, phxCmd ) =
               Phoenix.Socket.update msg model.phxSocket
       in
       ( { model | phxSocket = phxSocket }
       , Cmd.map PhoenixMsg phxCmd
       )

   ShowJoinedMessage channelName ->
       let
           _ =
               Debug.log "ShowJoinedMessage" ("joined " ++ channelName)
       in
       ( model, Cmd.none )

   ShowLeftMessage channelName ->
       let
           _ =
               Debug.log "Left channel" ("left " ++ channelName)
       in
       ( model
       , Cmd.none
       )

   ReceiveTeams raw ->
       let
           _ =
               Debug.log "ReceiveTeams" raw
       in
       -- case JD.decodeValue chatMessageDecoder raw of
       --     Ok chatMessage ->
       --         ( { model | messages = (chatMessage.user ++ ": " ++ chatMessage.body) :: model.messages }
       --         , Cmd.none
       --         )
       --
       --     Err error ->
       ( model, Cmd.none )
-}


updatePage : Context -> Page -> Msg -> Model -> ( Model, Cmd Msg, Context )
updatePage ctx page msg model =
    let
        toPage toModel toMsg subUpdate subMsg subModel =
            let
                ( newModel, newCmd, newCtx ) =
                    subUpdate ctx subMsg subModel
            in
            ( { model | pageState = toModel newModel, ctx = newCtx }, Cmd.map toMsg newCmd, newCtx )
    in
    case ( msg, page ) of
        ( LogoutClicked, _ ) ->
            toPage LoginPage LoginMsg LoginPage.update LoginPage.LogoutClicked User.initialModel

        ( TeamMsg subMsg, TeamPage subModel ) ->
            toPage TeamPage TeamMsg TeamPage.update subMsg subModel

        ( OnboardingMsg subMsg, OnboardingPage subModel ) ->
            toPage OnboardingPage OnboardingMsg OnboardingPage.update subMsg subModel

        ( HideToast, _ ) ->
            let
                newCtx =
                    { ctx | errorMsg = Nothing }
            in
            ( model, Cmd.none, newCtx )

        ( LoginMsg subMsg, LoginPage subModel ) ->
            toPage LoginPage LoginMsg LoginPage.update subMsg subModel

        ( SignupMsg subMsg, SignupPage subModel ) ->
            toPage SignupPage SignupMsg SignupPage.update subMsg subModel

        ( CreateChannelMsg subMsg, CreateChannelPage subModel ) ->
            toPage CreateChannelPage CreateChannelMsg CreateChannelPage.update subMsg subModel

        ( HideSwitchTeamsTT, _ ) ->
            ( model, Ports.setHideSwitchTeamsTT (), { ctx | showSwitchTeamsTooltip = False } )

        {-

           ( FetchTeams, _ ) ->
               let
                   payload =
                       JE.object [ ( "user", JE.string "user" ), ( "body", JE.string "model.newMessage" ) ]

                   push_ =
                       Phoenix.Push.init "inbox:fetch_teams" "inbox"
                           |> Phoenix.Push.withPayload payload

                   ( phxSocket, phxCmd ) =
                       Phoenix.Socket.push push_ model.phxSocket
               in
               ( ( { model | phxSocket = phxSocket }, Cmd.map PhoenixMsg phxCmd )
               , ctx
               )

              ( PhoenixMsg msg, _ ) ->
                  ( noChange, ctx )

              ( ShowJoinedMessage _, _ ) ->
                  ( noChange, ctx )

              ( JoinInboxChannel, _ ) ->
                  let
                      channel =
                          Phoenix.Channel.init "inbox"
                              |> Phoenix.Channel.withPayload
                                  (JE.object
                                      [ ( "user", JE.string "user" )
                                      , ( "body", JE.string "model.newMessage" )
                                      ]
                                  )
                              |> Phoenix.Channel.onJoin (always (ShowJoinedMessage "inbox"))
                              |> Phoenix.Channel.onClose (always (ShowLeftMessage "inbox"))

                      ( phxSocket, phxCmd ) =
                          Phoenix.Socket.join channel model.phxSocket
                  in
                  ( ( { model | phxSocket = phxSocket }, Cmd.map PhoenixMsg phxCmd )
                  , ctx
                  )
        -}
        x ->
            let
                _ =
                    Debug.log "updatePage unhandled msg, page: " ( msg, page )

                -- _ =
                --     Debug.log "updatePage unhandled page: " page
            in
            ( model, Cmd.none, ctx )


gotoRoute : Route -> Model -> ( Model, Cmd Msg )
gotoRoute route model =
    let
        setPageState state =
            { model | pageState = state }

        setRoute route_ model_ =
            { model_ | route = route_ }
    in
    case Debug.log "route" route of
        TeamRoute id ->
            let
                ( subModel, fx ) =
                    case model.pageState of
                        TeamPage existingModel ->
                            TeamPage.initRetainModel existingModel id model.ctx

                        _ ->
                            TeamPage.init id model.ctx
            in
            ( setPageState (TeamPage subModel) |> setRoute route
            , Cmd.batch [ Cmd.map TeamMsg fx ]
            )

        -- InboxRoute ->
        --     let
        --         ( subModel, fx ) =
        --             TeamPage.initAndGetTeams model.ctx
        --     in
        --     ( setPageState (TeamPage subModel)
        --     , Cmd.batch [ Cmd.map TeamMsg fx ]
        --     )
        LoginRoute ->
            ( setPageState (LoginPage User.initialModel) |> setRoute route
            , Cmd.map LoginMsg (LoginPage.ping model.ctx)
            )

        ProfileRoute ->
            ( setPageState (ProfilePage (Profile.initialModel model.ctx)) |> setRoute route
            , Cmd.none
            )

        SignupRoute ->
            ( setPageState (SignupPage Signup.initialModel) |> setRoute route
            , Cmd.none
            )

        HomeRoute ->
            let
                ( subModel, newCmd ) =
                    HomePage.init model.ctx
            in
            ( setPageState (HomePage subModel) |> setRoute route
            , Cmd.map HomeMsg newCmd
            )

        OnboardingRoute ->
            let
                subModel =
                    OnboardingPage.init
            in
            ( setPageState (OnboardingPage subModel) |> setRoute route
            , Cmd.none
            )

        CreateChannelRoute ->
            let
                ( subModel, subCmd ) =
                    CreateChannelPage.init model.ctx
            in
            ( setPageState (CreateChannelPage subModel) |> setRoute route
            , Cmd.map CreateChannelMsg subCmd
            )

        NotFoundRoute ->
            ( setPageState NotFoundPage |> setRoute route
            , Cmd.none
            )



-- view


view : Model -> Browser.Document Msg
view model =
    case model.pageState of
        page ->
            { title = "Warp"
            , body =
                [ div [ class "appx" ]
                    [ viewPage
                        page
                        model
                    ]
                ]
            }


layout : Model -> Html Msg -> Html Msg
layout model content =
    div []
        [ navBar model
        , toast model
        , content
        ]


layoutLogin : Model -> Html Msg -> Html Msg
layoutLogin model content =
    div [ id "login" ]
        [ nav [ class "navbar has-shadow is-fixed-top" ]
            [ div [ class "container" ]
                [ div [ class "navbar-brand" ]
                    [ a [ class "navbar-item", href "../", rel "noreferrer" ]
                        [ h1 []
                            [ text "Warp" ]
                        ]
                    ]
                ]
            ]
        , toast model
        , content
        ]


toast : Model -> Html Msg
toast model =
    case model.ctx.errorMsg of
        Just string ->
            div [ class "toast toast-enter-active" ]
                [ span [ class "tag is-danger" ]
                    [ i [ class "fa fa-check" ]
                        []
                    ]
                , div [ class "toast-title" ]
                    [ p []
                        [ text string ]
                    ]
                ]

        Nothing ->
            div [] []



--delay : Time -> msg -> Cmd msg
--delay time msg =
--    Process.sleep time
--        |> Task.andThen (always <| Task.succeed msg)
--        |> Task.perform identity


navBar : Model -> Html Msg
navBar model =
    let
        currentTeamName team =
            team |> Maybe.map .name |> Maybe.withDefault "No Team"

        loginLogout =
            case model.ctx.token of
                Just token ->
                    div [ class "navbar-dropdown  is-right" ]
                        [ a
                            [ class "navbar-item"
                            , rel "noreferrer"
                            , href "/#/profile"
                            ]
                            [ "Welcome "
                                ++ token.data.name
                                |> text
                            ]
                        , span [ class "navbar-item silent" ] [ b [] [ text "Switch Team" ] ]
                        , span []
                            (List.map
                                (\team ->
                                    a
                                        [ class "navbar-item"
                                        , rel "noreferrer"
                                        , href ("/#/teams/" ++ String.fromInt team.id)
                                        ]
                                        [ text (String.fromChar (Char.fromCode 187))
                                        , text (" " ++ team.name)
                                        ]
                                )
                                model.ctx.teams
                            )
                        , hr [ class "navbar-divider" ]
                            []
                        , a [ onClick LogoutClicked, class "navbar-item" ]
                            [ text "Logout" ]

                        -- , hr [ class "navbar-divider" ]
                        --     []
                        -- , a [ onClick JoinInboxChannel, class "navbar-item" ]
                        --     [ text "Join Inbox Channel" ]
                        -- , a [ onClick FetchTeams, class "navbar-item" ]
                        --     [ text "Fetch Teams" ]
                        ]

                Nothing ->
                    div [ class "navbar-dropdown" ]
                        [ a
                            [ class "navbar-item"
                            , rel "noreferrer"
                            , href "/#/login"
                            ]
                            [ "Login"
                                |> text
                            ]
                        ]
    in
    nav [ class "navbar has-shadow is-fixed-top" ]
        [ div [ class "container" ]
            [ div [ class "navbar-brand" ]
                [ a [ class "navbar-item", href "/", rel "noreferrer" ]
                    [ h1 []
                        [ text "Warp" ]
                    ]
                , div [ class "navbar-burger burger", attribute "data-target" "navMenu" ]
                    [ span []
                        []
                    , span []
                        []
                    , span []
                        []
                    ]
                ]
            , div [ class "navbar-menu", id "navMenu" ]
                [ div [ class "navbar-end" ]
                    [ div [ class "navbar-item" ]
                        [ currentTeamName model.ctx.currentTeam
                            |> text
                        ]
                    , div [ class "navbar-item" ]
                        [ if model.ctx.showSwitchTeamsTooltip then
                            div [ class " switch-team" ]
                                [ a [ class "delete  is-small", onClick HideSwitchTeamsTT ] []
                                , text "Switch teams from here "
                                , span [ class "fa fa-arrow-right" ] []
                                ]

                          else
                            Html.text ""
                        ]
                    , div [ class "navbar-item has-dropdown  is-hoverable" ]
                        [ a [ class "navbar-link", rel "noreferrer" ]
                            [ text "Account" ]
                        , loginLogout
                        ]
                    ]
                ]
            ]
        ]


viewPage : Page -> Model -> Html Msg
viewPage page model =
    case page of
        TeamPage subModel ->
            TeamPage.view subModel
                |> Html.map TeamMsg
                |> layout model

        LoginPage subModel ->
            LoginPage.view subModel
                |> Html.map LoginMsg
                |> layoutLogin model

        SignupPage subModel ->
            SignupPage.view subModel
                |> Html.map SignupMsg
                |> layoutLogin model

        HomePage subModel ->
            HomePage.view subModel
                |> Html.map HomeMsg
                |> layout model

        OnboardingPage subModel ->
            OnboardingPage.view subModel
                |> Html.map OnboardingMsg
                |> layout model

        CreateChannelPage subModel ->
            CreateChannelPage.view subModel
                |> Html.map CreateChannelMsg
                |> layout model

        ProfilePage subModel ->
            ProfilePage.view subModel
                |> Html.map ProfileMsg
                |> layout model

        NotFoundPage ->
            Page.NotFound.view
                |> layout model
