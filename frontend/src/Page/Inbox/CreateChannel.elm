module Page.Inbox.CreateChannel exposing (Model, Msg(..), createChannel, handleCreateNewChannelResponse, init, renderNewChannelModal, subscriptions, update, view)

-- import Api.Onboarding as Onboarding

import Api
import Api.Team
import Browser.Navigation as Navigation
import Html exposing (..)
import Html.Attributes as HA exposing (..)
import Html.Events as HE exposing (onClick, onInput)
import Http
import Json.Encode as JE exposing (..)
import RemoteData exposing (..)
import RemoteData.Http
import Types exposing (..)
import Utils


type alias Model =
    { defaultChannels : List String
    , channels : List Api.Team.Channel
    , customChannel : ( Bool, String )
    , teamId : Int
    }


type Msg
    = CreateCustomChannel
    | CreateDefaultChannel String
    | CustomChannelNameChanged String
    | OnCreateCustomChannelResp (WebData (Result Http.Error Api.Team.Channel))
    | OnCreateDefaultChannelResp (WebData (Result Http.Error Api.Team.Channel))
    | OnGetChannelsResp (WebData (Result Http.Error (List Api.Team.Channel)))
    | ToggleCustomChannel
    | OnNext


init : Context -> ( Model, Cmd Msg )
init ctx =
    let
        teamId =
            ctx.currentTeam
                |> Maybe.map .id
                |> Maybe.withDefault 0
    in
    ( { defaultChannels = [ "Marketing", "HR", "Design" ]
      , channels = []
      , customChannel = ( False, "" )
      , teamId = teamId
      }
    , getChannels teamId ctx
    )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


update : Context -> Msg -> Model -> ( Model, Cmd Msg, Context )
update ctx msg model =
    case msg of
        CreateCustomChannel ->
            let
                newCmd =
                    createChannel (Tuple.second model.customChannel) model ctx OnCreateCustomChannelResp
            in
            ( { model | customChannel = Tuple.mapSecond (\_ -> "") model.customChannel }, newCmd, ctx )

        CreateDefaultChannel name ->
            let
                newCmd =
                    createChannel name model ctx OnCreateDefaultChannelResp
            in
            ( model, newCmd, ctx )

        CustomChannelNameChanged str ->
            ( { model | customChannel = Tuple.mapSecond (\_ -> str) model.customChannel }, Cmd.none, ctx )

        OnCreateDefaultChannelResp data ->
            handleCreateNewChannelResponse model ctx data

        OnCreateCustomChannelResp data ->
            handleCreateNewChannelResponse { model | customChannel = Tuple.mapFirst (\_ -> False) model.customChannel } ctx data

        ToggleCustomChannel ->
            ( { model | customChannel = Tuple.mapFirst (\x -> not x) model.customChannel }
            , Cmd.none
            , ctx
            )

        OnGetChannelsResp response ->
            case response of
                RemoteData.Success (Ok data) ->
                    ( { model | channels = data } |> deleteDefaultChannelIfExists, Cmd.none, ctx )

                RemoteData.Success (Err errors) ->
                    ( model, Cmd.none, { ctx | errorMsg = Just (Debug.toString errors) } )

                RemoteData.Failure err ->
                    ( model, Cmd.none, { ctx | errorMsg = Just (Debug.toString err) } )

                _ ->
                    Utils.debugResponse response model ctx

        OnNext ->
            ( model, Navigation.pushUrl ctx.key ("/#/teams/" ++ String.fromInt model.teamId), ctx )


deleteDefaultChannelIfExists : Model -> Model
deleteDefaultChannelIfExists model =
    let
        channels =
            List.map .name model.channels

        newChannels =
            List.filter (\c -> not <| List.member c channels) model.defaultChannels
    in
    { model | defaultChannels = newChannels }


view : Model -> Html Msg
view model =
    section [ class "section" ]
        [ div [ class "container" ]
            [ h1 [ class "title" ] []
            , h2 [ class "subtitle" ] []
            , p [] []
            , div [ class "content" ]
                [ h2 [ class "title" ] [ text "Your teams:" ]
                , renderNewChannelModal model
                ]
            ]
        ]


renderNewChannelModal : Model -> Html Msg
renderNewChannelModal model =
    let
        renderChannel btn name =
            tr []
                [ td [] []
                , td [] [ text name ]
                , td []
                    [ if btn then
                        button [ class "button is-pulled-right", onClick (CreateDefaultChannel name) ] [ text "Create Channel" ]

                      else
                        text ""
                    ]
                ]
    in
    div
        [ class "modal is-active"
        ]
        [ div [ class "modal-background" ]
            []
        , div [ class "modal-content" ]
            [ header [ class "modal-card-head" ]
                [ p [ class "modal-card-title" ]
                    [ text "Create channels?" ]
                ]
            , section [ class "modal-card-body" ]
                [ table []
                    (List.map (\channel -> renderChannel False channel.name) model.channels
                        ++ List.map (renderChannel True) model.defaultChannels
                    )
                , case Tuple.first model.customChannel of
                    False ->
                        a [ onClick ToggleCustomChannel ] [ text "Add a channel" ]

                    True ->
                        div [ class "field is-horizontal" ]
                            [ div [ class "field-label is-normal" ]
                                [ label [ class "label" ]
                                    [ text "Channel name" ]
                                ]
                            , div [ class "field-body" ]
                                [ div [ class "field  has-addons" ]
                                    [ p [ class "control" ]
                                        [ input
                                            [ class "input"
                                            , placeholder "e.g. Lakme"
                                            , type_ "text"
                                            , onInput CustomChannelNameChanged
                                            , HA.value (Tuple.second model.customChannel)
                                            ]
                                            []
                                        ]
                                    , p [ class "control" ]
                                        [ button [ class "button is-success", onClick CreateCustomChannel, disabled (Utils.isBlank (Tuple.second model.customChannel)) ]
                                            [ text "Create Channel" ]
                                        ]
                                    ]
                                ]
                            ]
                ]
            , footer [ class "modal-card-foot" ]
                [ button [ class "button is-success", onClick OnNext ]
                    [ text "Next" ]
                ]
            ]
        ]


handleCreateNewChannelResponse : Model -> Context -> WebData (Result Http.Error Api.Team.Channel) -> ( Model, Cmd Msg, Context )
handleCreateNewChannelResponse model ctx data =
    let
        successResponse =
            case data of
                RemoteData.Success (Ok d) ->
                    ( { model | channels = d :: model.channels } |> deleteDefaultChannelIfExists, Cmd.none, ctx )

                RemoteData.Success (Err errors) ->
                    ( model, Cmd.none, { ctx | errorMsg = Just (Debug.toString errors) } )

                RemoteData.Failure err ->
                    ( model, Cmd.none, { ctx | errorMsg = Just (Debug.toString err) } )

                _ ->
                    Utils.debugResponse data model ctx
    in
    Utils.showErrorIfFailed data
        model
        ctx
        successResponse


createChannel : String -> Model -> Context -> (WebData (Result Http.Error Api.Team.Channel) -> Msg) -> Cmd Msg
createChannel name model ctx msg =
    let
        channel =
            { team_id = model.teamId
            , name = name
            }
    in
    case ctx.token of
        Just token ->
            Api.post "/channels"
                |> Api.withToken token.meta.token
                |> Api.withJsonBody (JE.object [ ( "channel", Api.Team.encodeChannel channel ) ])
                |> Api.withJsonResponse Api.Team.decodeChannel
                |> Api.withErrorHandler msg

        Nothing ->
            Navigation.pushUrl ctx.key "/#/login"


getChannels : Int -> Context -> Cmd Msg
getChannels id ctx =
    case ctx.token of
        Just token ->
            Api.get ("/teams/" ++ String.fromInt id ++ "/channels")
                |> Api.withToken token.meta.token
                |> Api.withJsonResponse Api.Team.decodeChannels
                |> Api.withErrorHandler OnGetChannelsResp

        Nothing ->
            Navigation.pushUrl ctx.key "/#/login"
