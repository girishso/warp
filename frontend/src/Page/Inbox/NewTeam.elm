module Page.Inbox.NewTeam exposing (Model, Msg(..), init, renderNewTeamModal, subscriptions, update, view)

-- import Api.Onboarding as Onboarding

import Api
import Api.Team
import Browser.Navigation as Navigation
import Html exposing (..)
import Html.Attributes as HA exposing (..)
import Html.Events as HE exposing (onClick, onInput)
import Json.Encode as JE exposing (..)
import RemoteData exposing (..)
import RemoteData.Http
import Types exposing (..)
import Utils


type alias Model =
    { name : String, email : String }


type Msg
    = CreateNewTeam
    | NewTeamNameChanged String
    | OnCreateTeamResp (WebData Team)


init : Model
init =
    Model "" ""


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


update : Context -> Msg -> Model -> ( Model, Cmd Msg, Context )
update ctx msg model =
    case msg of
        CreateNewTeam ->
            createTeam model ctx

        NewTeamNameChanged str ->
            ( { model | name = str }, Cmd.none, ctx )

        OnCreateTeamResp data ->
            handleCreateNewTeamResponse model ctx data


view : Model -> Html Msg
view model =
    section [ class "section" ]
        [ div [ class "container" ]
            [ h1 [ class "title" ] [ text model.name ]
            , h2 [ class "subtitle" ] [ text model.email ]
            , p [] []
            , div [ class "content" ]
                [ h2 [ class "title" ] [ text "Your teams:" ]
                , renderNewTeamModal model
                ]
            ]
        ]


renderNewTeamModal : Model -> Html Msg
renderNewTeamModal model =
    div
        [ class "modal is-active"
        ]
        [ div [ class "modal-background" ]
            []
        , div [ class "modal-content" ]
            [ header [ class "modal-card-head" ]
                [ p [ class "modal-card-title" ]
                    [ text "What do you want call your team?" ]
                ]
            , section [ class "modal-card-body" ]
                [ div [ class "field " ]
                    [ label [ class "label" ]
                        [ text "Your Team Name" ]
                    , div [ class "field " ]
                        [ div [ class "control" ]
                            [ input
                                [ class "input"
                                , placeholder "e.g. Lakme"
                                , type_ "text"
                                , onInput NewTeamNameChanged
                                , HA.value model.name
                                ]
                                []
                            ]
                        ]
                    ]
                ]
            , footer [ class "modal-card-foot" ]
                [ button [ class "button is-success", onClick CreateNewTeam, disabled (Utils.isBlank model.name) ]
                    [ text "Create Team" ]
                ]
            ]
        ]


handleCreateNewTeamResponse : Model -> Context -> WebData Team -> ( Model, Cmd Msg, Context )
handleCreateNewTeamResponse model ctx data =
    let
        successResponse =
            ( model
            , Navigation.pushUrl ctx.key "/#/createchannel"
            , { ctx
                | teams =
                    case data of
                        Success d ->
                            d :: ctx.teams

                        _ ->
                            ctx.teams
                , currentTeam =
                    case data of
                        Success d ->
                            Just d

                        _ ->
                            Nothing
              }
            )
    in
    Utils.showErrorIfFailed data
        model
        ctx
        successResponse


createTeam : Model -> Context -> ( Model, Cmd Msg, Context )
createTeam model ctx =
    let
        team =
            { name = model.name }

        newCmd =
            case ctx.token of
                Just token ->
                    RemoteData.Http.postWithConfig
                        (Utils.httpConfig token.meta.token)
                        (ctx.apiRoot ++ "/teams")
                        OnCreateTeamResp
                        Api.Team.decodeTeam
                        (JE.object [ ( "team", Api.Team.encodeTeam team ) ])

                Nothing ->
                    Navigation.pushUrl ctx.key "/#/login"
    in
    ( model, newCmd, ctx )



--
-- renderJoinChannelsModal : Model -> Html Msg
-- renderJoinChannelsModal model =
--     div
--         [ class
--             (case model.isJoinChannelsModalActive of
--                 True ->
--                     "modal is-active"
--
--                 False ->
--                     "modal"
--             )
--         ]
--         [ div [ class "modal-background" ]
--             []
--         , div [ class "modal-content" ]
--             [ header [ class "modal-card-head" ]
--                 [ p [ class "modal-card-title" ]
--                     [ text "Create channels for your team" ]
--                 ]
--             , section [ class "modal-card-body" ]
--                 [ div [ class "field " ]
--                     [ label [ class "label" ]
--                         [ text "Your Team Name" ]
--                     , div [ class "field " ]
--                         [ div [ class "control" ]
--                             [ input
--                                 [ class "input"
--                                 , placeholder "e.g. Lakme"
--                                 , type_ "text"
--                                 , onInput NewTeamNameChanged
--                                 , Html.Attributes.value model.newTeamName
--                                 ]
--                                 []
--                             ]
--                         ]
--                     ]
--                 ]
--             , footer [ class "modal-card-foot" ]
--                 [ button [ class "button is-success", onClick CreateNewChannels ]
--                     [ text "Create Channels" ]
--                 ]
--             ]
--         ]
