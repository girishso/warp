module Page.Inbox.Onboarding exposing (Model, Msg(..), State(..), Steps(..), init, subscriptions, update, view)

-- import Api.Onboarding as Onboarding

import Api
import Api.Team
import Browser.Navigation as Navigation
import Html exposing (..)
import Html.Attributes as HA exposing (..)
import Html.Events as HE exposing (onClick, onInput)
import Page.Inbox.JoinChannel as JoinChannel
import Page.Inbox.NewTeam as NewTeam
import Types exposing (..)


type alias Model =
    { newTeamState : NewTeam.Model, joinChannelState : JoinChannel.Model, current : Steps }


type Msg
    = Next
    | NewTeamMsg NewTeam.Msg
    | JoinChannelMsg JoinChannel.Msg


type State
    = NewTeamState


type Steps
    = NewTeamStep
    | JoinChannelStep


init : Model
init =
    { newTeamState = NewTeam.init, joinChannelState = JoinChannel.init, current = NewTeamStep }


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


update : Context -> Msg -> Model -> ( Model, Cmd Msg, Context )
update ctx msg model =
    case msg of
        Next ->
            let
                newModel =
                    case model.current of
                        NewTeamStep ->
                            { model | current = JoinChannelStep }

                        JoinChannelStep ->
                            model
            in
            ( newModel, Cmd.none, ctx )

        NewTeamMsg subMsg ->
            let
                ( newSubModel, newCmd, newCtx ) =
                    NewTeam.update ctx subMsg model.newTeamState
            in
            ( { model | newTeamState = newSubModel }, Cmd.map NewTeamMsg newCmd, newCtx )

        JoinChannelMsg subMsg ->
            let
                ( newSubModel, newCmd, newCtx ) =
                    JoinChannel.update ctx subMsg model.joinChannelState
            in
            ( { model | joinChannelState = newSubModel }, Cmd.map JoinChannelMsg newCmd, newCtx )


view : Model -> Html Msg
view model =
    section [ class "section" ]
        [ div [ class "container" ]
            [ h1 [ class "title" ] [ text "Onboarding" ]
            , h2 [ class "subtitle" ] [ text "model.email" ]
            , p [] []
            , div [ class "content" ]
                [ case model.current of
                    NewTeamStep ->
                        NewTeam.view model.newTeamState |> Html.map NewTeamMsg

                    JoinChannelStep ->
                        JoinChannel.view model.joinChannelState |> Html.map JoinChannelMsg
                ]
            ]
        ]
