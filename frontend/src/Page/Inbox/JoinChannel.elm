module Page.Inbox.JoinChannel exposing (Model, Msg(..), init, renderNewTeamModal, subscriptions, update, view)

-- import Api.Onboarding as Onboarding

import Api
import Api.Team
import Browser.Navigation as Navigation
import Html exposing (..)
import Html.Attributes as HA exposing (..)
import Html.Events as HE exposing (onClick, onInput)
import Types exposing (..)


type alias Model =
    {}


type Msg
    = NoOp


init =
    {}


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


update : Context -> Msg -> Model -> ( Model, Cmd Msg, Context )
update ctx msg model =
    ( model, Cmd.none, ctx )


view : Model -> Html Msg
view model =
    section [ class "section" ]
        [ div [ class "container" ]
            [ p [] []
            , div [ class "content" ]
                [ h2 [ class "title" ] [ text "Your teams:" ]
                ]
            ]
        ]


renderNewTeamModal : Model -> Html Msg
renderNewTeamModal model =
    div
        [ class "modal is-active" ]
        [ div [ class "modal-background" ]
            []
        , div [ class "modal-content" ]
            [ header [ class "modal-card-head" ]
                [ p [ class "modal-card-title" ]
                    [ text "What do you want call your team?" ]
                ]
            , section [ class "modal-card-body" ]
                [ div [ class "field " ]
                    [ label [ class "label" ]
                        [ text "Your Team Name" ]
                    , div [ class "field " ]
                        [ div [ class "control" ]
                            [ input
                                [ class "input"
                                , placeholder "e.g. Lakme"
                                , type_ "text"

                                -- , onInput NewTeamNameChanged
                                -- , HA.value model.newTeamName
                                ]
                                []
                            ]
                        ]
                    ]
                ]
            , footer [ class "modal-card-foot" ]
                [ button
                    [ class "button is-success"

                    -- , onClick CreateNewTeam
                    ]
                    [ text "Create Team" ]
                ]
            ]
        ]



--
-- renderJoinChannelsModal : Model -> Html Msg
-- renderJoinChannelsModal model =
--     div
--         [ class
--             (case model.isJoinChannelsModalActive of
--                 True ->
--                     "modal is-active"
--
--                 False ->
--                     "modal"
--             )
--         ]
--         [ div [ class "modal-background" ]
--             []
--         , div [ class "modal-content" ]
--             [ header [ class "modal-card-head" ]
--                 [ p [ class "modal-card-title" ]
--                     [ text "Create channels for your team" ]
--                 ]
--             , section [ class "modal-card-body" ]
--                 [ div [ class "field " ]
--                     [ label [ class "label" ]
--                         [ text "Your Team Name" ]
--                     , div [ class "field " ]
--                         [ div [ class "control" ]
--                             [ input
--                                 [ class "input"
--                                 , placeholder "e.g. Lakme"
--                                 , type_ "text"
--                                 , onInput NewTeamNameChanged
--                                 , Html.Attributes.value model.newTeamName
--                                 ]
--                                 []
--                             ]
--                         ]
--                     ]
--                 ]
--             , footer [ class "modal-card-foot" ]
--                 [ button [ class "button is-success", onClick CreateNewChannels ]
--                     [ text "Create Channels" ]
--                 ]
--             ]
--         ]
