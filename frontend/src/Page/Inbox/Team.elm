module Page.Inbox.Team exposing (Msg(..), addActiveClassIfEql, avatar, buildMessage, createNewChannel, getFirstTeamId, getFirstThread, getInbox, getInboxTask, getInboxTask2, getTeams, getTeamsAndInbox, getTeamsTask, getTeamsTask2, getThingNameFromId, handleCreateNewChannelResponse, handleGetInboxResponse, handleGetTeamsResponse, handlePostNewMessageResponse, handlePostNewThreadResponse, init, initAndGetTeams, initRetainModel, markThreadRead, pickFirst, postNewMessage, postNewThread, postReadThread, renderAThread, renderChannels, renderInbox, renderLastPane, renderMessages, renderNewMessageActions, renderNewMessageBox, renderNewThreadForm, renderThreadChannelTag, renderThreadLastMessage, renderThreads, setCurrentThread, setInboxAndTeams, update, view)

-- import Json.Decode as JD
-- import Json.Decode.Pipeline as Pipeline exposing (optional, required)

import Api
import Api.Team exposing (..)
import Browser.Navigation as Navigation
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Html.Keyed as Keyed exposing (..)
import Http exposing (Header)
import Json.Encode as JE exposing (..)
import Ports exposing (..)
import RemoteData exposing (..)
import RemoteData.Http
import Task
import Types exposing (..)
import Utils exposing (..)


init : String -> Context -> ( Model, Cmd Msg )
init id ctx =
    ( initialModel
      -- , Cmd.batch [ getInbox (Maybe.withDefault 0 (String.toInt id)) ctx, getTeams ctx ]
    , getTeamsAndInbox ctx
    )


initRetainModel : Model -> String -> Context -> ( Model, Cmd Msg )
initRetainModel model id ctx =
    ( { model | current_channel_id = Nothing }
    , Cmd.batch
        [ getInbox (Maybe.withDefault 0 (String.toInt id)) ctx
        , getTeams ctx
        ]
    )


initAndGetTeams : Context -> ( Model, Cmd Msg )
initAndGetTeams ctx =
    ( initialModel
      -- , Cmd.batch [ getTeams ctx ]
    , getTeamsAndInbox ctx
    )



-- Msg


type Msg
    = OnGetInboxResp (WebData (Result Http.Error Warp))
    | OnGetInboxAndTeamsResp (WebData (Result Http.Error ( Warp, List Team )))
    | OnGetTeamsResp (WebData (Result Http.Error (List Team)))
    | OnPostNewThreadResp (WebData ThreadNMessage)
    | OnPostReadThreadResp (WebData String)
    | OnPostNewMessageResp (WebData (Result Http.Error Message))
    | OnCreateNewChannelResp (WebData Channel)
    | OnThreadClick Int
    | OnChannelClick (Maybe Int)
    | OnPostNewThread
    | OnPostNewMessage
    | NewMessageBodyChanged String
    | NewThreadSubjectChanged String
    | NewChannelNameChanged String
    | CreateNewChannel
    | StartNewThreadClick
    | DiscardPostNewMessage
    | ToggleNewChannelModal



-- update
---- UPDATE ----


update : Context -> Msg -> Model -> ( Model, Cmd Msg, Context )
update ctx msg model =
    let
        noChange =
            ( model, Cmd.none )
    in
    case msg of
        OnGetInboxResp data ->
            handleGetInboxResponse model ctx data

        OnGetInboxAndTeamsResp data ->
            setInboxAndTeams model ctx data

        OnGetTeamsResp data ->
            handleGetTeamsResponse model ctx data

        OnThreadClick thread_id ->
            let
                thread =
                    model.inbox
                        |> Maybe.andThen
                            (\inbox ->
                                inbox.threads
                                    |> List.filter (\thread_ -> thread_.id == thread_id)
                                    |> List.head
                            )
            in
            ( setCurrentThread model
                (thread
                    |> Maybe.map .id
                    |> Maybe.withDefault 0
                )
            , Cmd.batch
                [ Ports.scrollTo "msg-wrapper"
                , thread
                    |> Maybe.map
                        (\t ->
                            if t.un_read then
                                Cmd.none

                            else
                                postReadThread t.id ctx
                        )
                    |> Maybe.withDefault Cmd.none
                ]
            , ctx
            )

        OnChannelClick channel_id ->
            ( { model
                | current_channel_id = channel_id
                , current_thread_id = 0
                , isStartNewThreadActive = False
              }
            , Cmd.none
            , ctx
            )

        NewMessageBodyChanged str ->
            ( { model
                | newMessageBody =
                    if isBlank str then
                        ""

                    else
                        str
              }
            , Cmd.none
            , ctx
            )

        OnPostNewMessage ->
            ( model, postNewMessage model ctx, ctx )

        DiscardPostNewMessage ->
            ( { model
                | newMessageBody = ""
                , isStartNewThreadActive = False
                , newThreadSubject = ""
              }
            , Cmd.none
            , ctx
            )

        ToggleNewChannelModal ->
            ( { model | isNewChannelModelActive = not model.isNewChannelModelActive }, Cmd.none, ctx )

        OnPostNewMessageResp data ->
            handlePostNewMessageResponse model ctx data

        OnCreateNewChannelResp data ->
            handleCreateNewChannelResponse model ctx data

        NewChannelNameChanged str ->
            ( { model | newChannelName = str }, Cmd.none, ctx )

        CreateNewChannel ->
            ( model, createNewChannel model ctx, ctx )

        StartNewThreadClick ->
            ( { model | isStartNewThreadActive = True }, Cmd.none, ctx )

        NewThreadSubjectChanged str ->
            ( { model
                | newThreadSubject =
                    if isBlank str then
                        ""

                    else
                        str
              }
            , Cmd.none
            , ctx
            )

        OnPostNewThread ->
            ( model, postNewThread model ctx, ctx )

        OnPostNewThreadResp data ->
            handlePostNewThreadResponse model ctx data

        OnPostReadThreadResp _ ->
            ( markThreadRead model, Cmd.none, ctx )


setInboxAndTeams : Model -> Context -> WebData (Result Http.Error ( Warp, List Team )) -> ( Model, Cmd Msg, Context )
setInboxAndTeams model ctx response =
    case response of
        RemoteData.Success (Ok ( inbox, teams )) ->
            let
                firstThread =
                    getFirstThread inbox

                newModel =
                    setCurrentThread
                        { model
                            | inbox = Just inbox
                            , isNewTeamModalActive = List.isEmpty teams
                        }
                        (firstThread
                            |> Maybe.map .id
                            |> Maybe.withDefault 0
                        )

                newCtx =
                    { ctx
                        | currentTeam = Just inbox.team
                        , teams = teams
                    }
            in
            ( newModel, Cmd.none, newCtx )

        RemoteData.Success (Err errors) ->
            ( model, Cmd.none, { ctx | errorMsg = Just (Debug.toString errors) } )

        _ ->
            debugResponse response model ctx


markThreadRead : Model -> Model
markThreadRead model =
    let
        markThreadRead_ thread_id threads =
            threads
                |> List.map
                    (\t ->
                        if t.id == thread_id then
                            { t | un_read = False }

                        else
                            t
                    )

        newInbox =
            Maybe.map
                (\warp -> { warp | threads = markThreadRead_ model.current_thread_id warp.threads })
                model.inbox
    in
    { model
        | inbox = newInbox
        , isStartNewThreadActive = False
    }


handleGetInboxResponse : Model -> Context -> WebData (Result Http.Error Warp) -> ( Model, Cmd Msg, Context )
handleGetInboxResponse model ctx data =
    let
        successResponse : Warp -> ( Model, Cmd Msg, Context )
        successResponse inbox =
            let
                firstThread =
                    getFirstThread inbox

                newModel =
                    setCurrentThread
                        { model | inbox = Just inbox }
                        (firstThread
                            |> Maybe.map .id
                            |> Maybe.withDefault 0
                        )

                newCtx =
                    { ctx
                        | currentTeam = Just inbox.team
                    }
            in
            ( newModel
            , Cmd.batch
                [ Ports.scrollTo "msg-wrapper"
                , case firstThread of
                    Just thread ->
                        postReadThread thread.id newCtx

                    Nothing ->
                        Cmd.none
                ]
            , newCtx
            )
    in
    showErrorIfFailed2 data
        model
        ctx
        successResponse


handleGetTeamsResponse : Model -> Context -> WebData (Result Http.Error (List Team)) -> ( Model, Cmd Msg, Context )
handleGetTeamsResponse model ctx data =
    let
        successResponse : List Team -> ( Model, Cmd Msg, Context )
        successResponse teams =
            let
                newModel =
                    { model
                        | isNewTeamModalActive = List.isEmpty teams
                    }

                newCtx =
                    { ctx | teams = teams }

                newCmd =
                    case model.inbox of
                        Nothing ->
                            let
                                team_id =
                                    getFirstTeamId teams ctx
                            in
                            if team_id /= 0 then
                                Cmd.batch
                                    [ -- getInbox team_id ctx
                                      Navigation.pushUrl ctx.key ("/#/teams/" ++ Debug.toString team_id)
                                    ]

                            else
                                Cmd.none

                        Just a ->
                            Cmd.none
            in
            ( newModel, newCmd, newCtx )
    in
    showErrorIfFailed2 data
        model
        ctx
        successResponse


handlePostNewMessageResponse : Model -> Context -> WebData (Result Http.Error Message) -> ( Model, Cmd Msg, Context )
handlePostNewMessageResponse model ctx data =
    let
        successResponse : Message -> ( Model, Cmd Msg, Context )
        successResponse message =
            let
                newInbox =
                    case model.inbox of
                        Just inbox ->
                            Just { inbox | messages = List.append inbox.messages [ message ] }

                        Nothing ->
                            Nothing
            in
            ( { model | newMessageBody = "", inbox = newInbox }
            , case ctx.currentTeam of
                Just team ->
                    Cmd.batch [ getInbox team.id ctx, Ports.scrollTo "msg-wrapper" ]

                Nothing ->
                    Cmd.none
            , ctx
            )
    in
    showErrorIfFailed2 data
        model
        ctx
        successResponse


handleCreateNewChannelResponse : Model -> Context -> WebData Channel -> ( Model, Cmd Msg, Context )
handleCreateNewChannelResponse model ctx data =
    let
        successResponse =
            ( { model
                | newMessageBody = ""
                , inbox =
                    case model.inbox of
                        Just inbox ->
                            case data of
                                Success d ->
                                    Just { inbox | channels = d :: inbox.channels }

                                _ ->
                                    Just inbox

                        Nothing ->
                            Nothing
                , isNewChannelModelActive = not model.isNewChannelModelActive
                , newChannelName = ""
              }
            , Cmd.none
            , ctx
            )
    in
    showErrorIfFailed data
        model
        ctx
        successResponse


handlePostNewThreadResponse : Model -> Context -> WebData ThreadNMessage -> ( Model, Cmd Msg, Context )
handlePostNewThreadResponse model ctx data =
    let
        successResponse =
            let
                ( newInbox, newThreadId ) =
                    case model.inbox of
                        Just inbox ->
                            case data of
                                Success d ->
                                    ( Just
                                        { inbox
                                            | messages = List.append inbox.messages [ d.message ]
                                            , threads = d.thread :: inbox.threads
                                        }
                                    , d.thread.id
                                    )

                                _ ->
                                    ( Just inbox, model.current_thread_id )

                        Nothing ->
                            ( Nothing, model.current_thread_id )
            in
            ( { model
                | inbox = newInbox
                , isStartNewThreadActive = False
                , newMessageBody = ""
                , current_thread_id = newThreadId
                , newThreadSubject = ""
              }
            , Cmd.none
            , ctx
            )
    in
    showErrorIfFailed data model ctx successResponse


setCurrentThread : Model -> Int -> Model
setCurrentThread model thread_id =
    { model | current_thread_id = thread_id, isStartNewThreadActive = False }


getFirstThread : Warp -> Maybe Thread
getFirstThread data =
    List.head data.threads


getFirstTeamId : List Team -> Context -> Int
getFirstTeamId teams ctx =
    case List.head teams of
        Nothing ->
            0

        Just team ->
            team.id



-- http Requests


createNewChannel : Model -> Context -> Cmd Msg
createNewChannel model ctx =
    let
        channel =
            { team_id =
                ctx.currentTeam
                    |> Maybe.map .id
                    |> Maybe.withDefault 0
            , name = model.newChannelName
            }
    in
    case ctx.token of
        Just token ->
            RemoteData.Http.postWithConfig
                (httpConfig token.meta.token)
                (ctx.apiRoot ++ "/channels")
                OnCreateNewChannelResp
                decodeChannel
                (JE.object [ ( "channel", encodeChannel channel ) ])

        Nothing ->
            Navigation.pushUrl ctx.key "/#/login"


postNewThread : Model -> Context -> Cmd Msg
postNewThread model ctx =
    let
        message =
            buildMessage model ctx

        thread =
            { team_id = message.team_id
            , channel_id = message.channel_id
            , user_id = message.sender_id
            , subject =
                if isBlank model.newThreadSubject then
                    "(no subject)"

                else
                    model.newThreadSubject
            , id = 0
            , inserted_at = ""
            , un_read = False
            }
    in
    case ctx.token of
        Just token ->
            RemoteData.Http.postWithConfig
                (httpConfig token.meta.token)
                (ctx.apiRoot ++ "/messages/threadswm")
                OnPostNewThreadResp
                decodeThreadNMessage
                (JE.object [ ( "message", encodeThreadNMessage { message = message, thread = thread } ) ])

        Nothing ->
            Navigation.pushUrl ctx.key "/#/login"


postNewMessage : Model -> Context -> Cmd Msg
postNewMessage model ctx =
    let
        message =
            buildMessage model ctx
    in
    case ctx.token of
        Just token ->
            Api.post "/messages"
                |> Api.withToken token.meta.token
                |> Api.withJsonBody (JE.object [ ( "message", encodeMessage message ) ])
                |> Api.withJsonResponse decodeMessage
                |> Api.withErrorHandler OnPostNewMessageResp

        Nothing ->
            Navigation.pushUrl ctx.key "/#/login"


postReadThread : Int -> Context -> Cmd Msg
postReadThread thread_id ctx =
    case ctx.token of
        Just token ->
            RemoteData.Http.postWithConfig
                (httpConfig token.meta.token)
                (ctx.apiRoot ++ "/teams/touch_thread")
                OnPostReadThreadResp
                decodeAlways
                (JE.object [ ( "thread_id", JE.int thread_id ) ])

        Nothing ->
            Navigation.pushUrl ctx.key "/#/login"


buildMessage : Model -> Context -> Message
buildMessage model ctx =
    { thread_id =
        model.current_thread_id
    , team_id =
        ctx.currentTeam
            |> Maybe.map .id
            |> Maybe.withDefault 0
    , sender_id =
        case ctx.token of
            Just token ->
                token.data.id

            Nothing ->
                0
    , recipient_ids = []
    , id = 0
    , channel_id =
        case model.current_channel_id of
            Just id ->
                id

            Nothing ->
                case model.inbox of
                    Just ib ->
                        case List.head ib.channels of
                            Just c ->
                                c.id

                            Nothing ->
                                0

                    Nothing ->
                        0
    , body = model.newMessageBody
    , inserted_at = ""
    , inserted_ago = ""
    }



-- getWithConfig     : Config -> String -> (WebData success -> msg) -> Decoder success -> Cmd msg
-- getTaskWithConfig : Config -> String -> Decoder success -> Task Never (WebData success)


getInboxTask : Int -> Context -> Task.Task Never (WebData Warp)
getInboxTask id ctx =
    RemoteData.Http.getTaskWithConfig
        (httpConfig "token.meta.token")
        (ctx.apiRoot ++ "/teams/" ++ String.fromInt id ++ "/inbox")
        decodeWarp


getTeamsTask : Context -> Task.Task Never (WebData (List Team))
getTeamsTask ctx =
    RemoteData.Http.getTaskWithConfig
        (httpConfig "token.meta.token")
        (ctx.apiRoot ++ "/teams")
        decodeTeams


getInbox : Int -> Context -> Cmd Msg
getInbox id ctx =
    case ctx.token of
        Just token ->
            Api.get ("/teams/" ++ String.fromInt id ++ "/inbox")
                |> Api.withToken token.meta.token
                |> Api.withJsonResponse decodeWarp
                |> Api.withErrorHandler OnGetInboxResp

        Nothing ->
            Navigation.pushUrl ctx.key "/#/login"


getTeams : Context -> Cmd Msg
getTeams ctx =
    case ctx.token of
        Just token ->
            Api.get "/teams"
                |> Api.withToken token.meta.token
                |> Api.withJsonResponse decodeTeams
                |> Api.withErrorHandler OnGetTeamsResp

        Nothing ->
            Navigation.pushUrl ctx.key "/#/login"


pickFirst : List Team -> Task.Task Http.Error ( Int, List Team )
pickFirst teams =
    case List.head teams of
        Just team ->
            Task.succeed ( team.id, teams )

        Nothing ->
            Task.fail Http.NetworkError


getTeamsTask2 : Context -> Task.Task Http.Error (List Team)
getTeamsTask2 ctx =
    Api.get "/teams"
        |> Api.withToken
            (case ctx.token of
                Nothing ->
                    ""

                Just token ->
                    token.meta.token
            )
        |> Api.withJsonResponse decodeTeams
        |> Api.toTask


getInboxTask2 : Context -> ( Int, List Team ) -> Task.Task Http.Error ( Warp, List Team )
getInboxTask2 ctx ( id, teams ) =
    Api.get ("/teams/" ++ String.fromInt id ++ "/inbox")
        |> Api.withToken
            (case ctx.token of
                Nothing ->
                    ""

                Just token ->
                    token.meta.token
            )
        |> Api.withJsonResponse decodeWarp
        |> Api.toTask
        |> Task.map (\warp -> ( warp, teams ))


getTeamsAndInbox : Context -> Cmd Msg
getTeamsAndInbox ctx =
    getTeamsTask2 ctx
        |> Task.andThen pickFirst
        -- |> Task.map List.head
        |> Task.andThen (getInboxTask2 ctx)
        |> Task.attempt (\x -> OnGetInboxAndTeamsResp (RemoteData.succeed x))



---- VIEW ----


view : Model -> Html Msg
view model =
    let
        inboxRendered =
            case model.inbox of
                Just inbox ->
                    renderInbox inbox model

                Nothing ->
                    h1 [] [ text "" ]
    in
    div [] [ inboxRendered ]


renderInbox : Warp -> Model -> Html Msg
renderInbox inbox model =
    div [ class "" ]
        [ div [ id "mail-app", class "columns" ]
            [ aside [ class "column is-2 aside hero is-fullheight" ]
                [ renderChannels inbox.channels model
                ]
            , div [ id "message-feed", class "column is-4 messages hero is-fullheight" ]
                [ renderThreads inbox model
                ]
            , div [ id "message-pane", class "column is-6 message hero is-fullheight" ]
                [ renderLastPane model inbox model.current_thread_id
                ]
            ]
        ]


renderChannels : List Channel -> Model -> Html Msg
renderChannels channels model =
    div [ class "main" ]
        [ a
            [ class
                (addActiveClassIfEql "item" model.current_channel_id Nothing)
            , onClick (OnChannelClick Nothing)
            ]
            [ span [ class "icon" ]
                [ i [ class "fa fa-inbox" ]
                    []
                ]
            , span [ class "name" ]
                [ text "Inbox" ]
            ]

        -- , a [ class "item", href "#", rel "noreferrer" ]
        --     [ span [ class "icon" ]
        --         [ i [ class "fa fa-star" ]
        --             []
        --         ]
        --     , span [ class "name" ]
        --         [ text "Starred" ]
        --     ]
        , div [ class "channels-separator", onClick ToggleNewChannelModal ]
            [ span [ class "" ] [ text "Channels" ]
            , a [ class "fr channel-plus" ] [ i [ class "fa fa-plus" ] [] ]
            ]
        , div
            [ class
                (case model.isNewChannelModelActive of
                    True ->
                        "modal is-active"

                    False ->
                        "modal"
                )
            ]
            [ div [ class "modal-background" ]
                []
            , div [ class "modal-content" ]
                [ header [ class "modal-card-head" ]
                    [ p [ class "modal-card-title" ]
                        [ text "Create new channel" ]
                    , button [ attribute "aria-label" "close", onClick ToggleNewChannelModal, class "delete" ]
                        []
                    ]
                , section [ class "modal-card-body" ]
                    [ div [ class "field is-horizontal" ]
                        [ div [ class "field-label  is-fullwidth" ]
                            [ label [ class "label" ]
                                [ text "Channel Name" ]
                            ]
                        , div [ class "field-body" ]
                            [ div [ class "field  is-narrow" ]
                                [ div [ class "control" ]
                                    [ input
                                        [ class "input"
                                        , placeholder "e.g. Design, Marketing"
                                        , type_ "text"
                                        , onInput NewChannelNameChanged
                                        , Html.Attributes.value model.newChannelName
                                        ]
                                        []
                                    ]
                                ]
                            ]
                        ]
                    ]
                , footer [ class "modal-card-foot" ]
                    [ button [ class "button is-success", onClick CreateNewChannel ]
                        [ text "Create Channel" ]
                    , button [ class "button", onClick ToggleNewChannelModal ]
                        [ text "Cancel" ]
                    ]
                ]
            ]
        , div []
            (List.map
                (\channel ->
                    span
                        [ Maybe.withDefault 0 model.current_channel_id
                            |> addActiveClassIfEql "item" channel.id
                            |> class
                        , onClick (OnChannelClick (Just channel.id))
                        ]
                        [ span [ class "icon" ]
                            [ i [ class "fa fa-angle-double-right" ]
                                []
                            ]
                        , span [ class "name" ]
                            [ text channel.name ]
                        ]
                )
                channels
            )
        ]


avatar : Bool -> String -> Html Msg
avatar smaller string =
    let
        width =
            if smaller then
                "25px "

            else
                "34px "

        radius =
            if smaller then
                "13px "

            else
                "17px "
    in
    div
        [ class "avatar"
        , style "flex-shrink" "0"
        , style "background-color" (getRandomColor string)
        , style "background-size" (width ++ width)
        , style "background-position" "center center"
        , style "color" "rgb(255, 255, 255)"
        , style "width" width
        , style "height" width
        , style "line-height" width
        , style "border-radius" radius
        , style "text-align" "center"
        , style "font-size" radius
        , style "font-weight" "bolder"
        ]
        [ String.left 1 string |> text ]


addActiveClassIfEql : String -> a -> a -> String
addActiveClassIfEql class_str id1 id2 =
    if id1 == id2 then
        class_str ++ " active"

    else
        class_str


renderLastPane : Model -> Warp -> Int -> Html Msg
renderLastPane model inbox current_thread_id =
    if model.isStartNewThreadActive then
        renderNewThreadForm model inbox

    else
        renderMessages model inbox current_thread_id


renderNewThreadForm : Model -> Warp -> Html Msg
renderNewThreadForm model inbox =
    div []
        [ div [ class "thread-subject-card subject-wrapper bs" ]
            [ div [ class "msg-card-inner" ]
                [ div [ class "msg-subject" ]
                    [ span [ class "msg-subject" ]
                        [ input
                            [ class "no-border"
                            , onInput NewThreadSubjectChanged
                            , placeholder "Subject..."
                            ]
                            []
                        ]
                    ]
                ]
            ]
        , div [ class "new-thread-new-msg-wrapper mt5" ]
            [ div [ class "msg-card-inner" ]
                [ textarea
                    [ class "no-border new-msg-ta fsn"
                    , onInput NewMessageBodyChanged
                    , placeholder "New comment.."
                    , model.newMessageBody |> Html.Attributes.value
                    ]
                    []
                ]
            , renderNewMessageActions model inbox True
            ]
        ]


renderNewMessageActions : Model -> Warp -> Bool -> Html Msg
renderNewMessageActions model inbox renderDiscard =
    div
        [ class "new-thread-controls" ]
        [ div [ class "channel-name" ]
            [ (case model.current_channel_id of
                Just id ->
                    getThingNameFromId inbox.channels id

                Nothing ->
                    "General"
              )
                |> text
            ]
        , div [ class "controls" ]
            [ div [ class "buttons" ]
                [ if renderDiscard then
                    button
                        [ class "button is-light is-medium"
                        , onClick DiscardPostNewMessage
                        ]
                        [ span [ class "icon is-small" ]
                            [ i [ class "fa fa-trash" ] []
                            ]
                        , span [] [ text "Discard" ]
                        ]

                  else
                    Html.text ""
                , button
                    [ class "button is-success is-pulled-right is-medium"
                    , onClick
                        (if renderDiscard then
                            OnPostNewThread

                         else
                            OnPostNewMessage
                        )
                    , disabled
                        (if isPresent model.newMessageBody then
                            if renderDiscard then
                                isBlank model.newThreadSubject

                            else
                                False

                         else
                            True
                        )
                    ]
                    [ span [ class "icon is-small" ]
                        [ i [ class "fa fa-paper-plane" ] []
                        ]
                    , span [] [ text "Post" ]
                    ]
                ]
            ]
        ]


renderMessages : Model -> Warp -> Int -> Html Msg
renderMessages model inbox current_thread_id =
    let
        messages =
            inbox.messages
                |> List.filter (\message -> message.thread_id == current_thread_id)

        renderAMessage =
            \message ->
                div [ class "message" ]
                    [ article [ class "media" ]
                        [ div [ class "media-left" ]
                            [ getThingNameFromId inbox.users message.sender_id
                                |> avatar False
                            ]
                        , div [ class "media-content" ]
                            [ div [ class "content" ]
                                [ p []
                                    [ strong []
                                        [ getThingNameFromId inbox.users message.sender_id |> text ]
                                    , text " "
                                    , small [ class "silent" ]
                                        [ text message.inserted_ago ]
                                    , br []
                                        []
                                    , text message.body
                                    ]
                                ]
                            ]
                        ]
                    , hr [] []
                    ]
    in
    div [ class "inbox-messages" ]
        [ div [ class "thread-subject-card bs" ]
            [ div
                [ class "msg-card-inner" ]
                [ div [ class "msg-subject" ]
                    [ span [ class "msg-subject" ]
                        [ if current_thread_id == 0 then
                            "No thread selected" |> text

                          else
                            inbox.threads
                                |> List.filter (\thing -> thing.id == current_thread_id)
                                |> List.head
                                |> Maybe.map .subject
                                |> Maybe.withDefault "No thread selected"
                                |> text
                        ]
                    ]
                ]
            ]
        , div [ id "msg-inv-wrapper" ] [ div [ id "msg-wrapper", class "mt5 pt25" ] (List.map renderAMessage messages) ]
        , if model.current_thread_id == 0 then
            div [] []

          else
            renderNewMessageBox model inbox
        ]


renderNewMessageBox : Model -> Warp -> Html Msg
renderNewMessageBox model inbox =
    div [ class "new-msg-wrapper mt5" ]
        [ div [ class "message" ]
            [ textarea
                [ class "new-msg-ta fsn"
                , onInput NewMessageBodyChanged
                , placeholder "New comment.."
                , model.newMessageBody |> Html.Attributes.value
                ]
                []
            , renderNewMessageActions model inbox False
            ]
        ]


getThingNameFromId : List { b | id : Int, name : String } -> Int -> String
getThingNameFromId things_list thing_id =
    let
        thing =
            List.filter (\thing_ -> thing_.id == thing_id) things_list
                |> List.head
    in
    case thing of
        Just t ->
            t.name

        Nothing ->
            "Anonymous"


renderThreads : Warp -> Model -> Html Msg
renderThreads inbox model =
    let
        listThreads =
            inbox.threads
                |> List.filter
                    (\thread ->
                        case model.current_channel_id of
                            Just current_channel_id ->
                                thread.channel_id == current_channel_id

                            Nothing ->
                                True
                    )
                |> List.map
                    (\thread -> renderAThread thread inbox model)
                |> Keyed.node "div" []

        lst =
            div [ id "tw" ]
                [ div
                    [ id "threads-wrapper", class "msgs bs mt5" ]
                    [ listThreads ]
                ]
    in
    div [ class "inbox-messages" ]
        [ div [ class "channel-name-card bs" ]
            [ div [ class "msg-card-inner" ]
                [ div [ class "msg-subject" ]
                    [ span [ class "msg-subject" ]
                        [ strong []
                            [ (case model.current_channel_id of
                                Just channel_id ->
                                    getThingNameFromId inbox.channels channel_id

                                Nothing ->
                                    "Inbox"
                              )
                                |> text
                            ]
                        ]
                    ]
                ]
            ]
        , div [ class "channel-name-card bs mt5 silent fsn " ]
            [ a [ class "start-new-thread", onClick StartNewThreadClick ]
                [ div [ class "msg-card-inner" ]
                    [ div [ class "msg-subject" ]
                        [ span [ class "msg-subject " ]
                            [ text "Start a new Thread"
                            , span [ class "fr" ] [ i [ class "fa fa-pencil-square-o fa-lg" ] [] ]
                            ]
                        ]
                    ]
                ]
            ]
        , lst
        ]


renderAThread : Thread -> Warp -> Model -> ( String, Html Msg )
renderAThread thread inbox model =
    ( String.fromInt thread.id
    , article
        [ classList
            [ ( "media", True )
            , ( "hilite", thread.id == model.current_thread_id )
            ]
        , onClick (OnThreadClick thread.id)
        ]
        [ div [ class "media-content" ]
            [ div [ class "content" ]
                [ p []
                    [ strong []
                        [ text thread.subject ]
                    , small [ class "is-pulled-right ts" ]
                        [ span [ class "align" ] [ text (getThingNameFromId inbox.users thread.user_id) ]
                        , span [ class "add-label" ] [ text (getThingNameFromId inbox.users thread.user_id) ]
                        ]
                    , br []
                        []
                    , renderThreadLastMessage model inbox thread
                    ]
                ]
            , nav [ class "level" ]
                [ div [ class "level-left" ]
                    [ renderThreadChannelTag model inbox thread
                    ]
                ]
            ]

        -- , div [ class "media-right" ]
        --     [ button [ class "delete" ]
        --         []
        --     ]
        , figure [ class "media-right" ]
            [ p [ class "image is-32x32" ]
                [ getThingNameFromId inbox.users thread.user_id
                    |> avatar True
                ]
            ]
        ]
    )


renderThreadLastMessage : Model -> Warp -> Thread -> Html Msg
renderThreadLastMessage model inbox thread =
    div [ class "thread-first-msg has-text-grey" ]
        [ inbox.messages
            |> List.filter (\thread_ -> thread_.thread_id == thread.id)
            |> lastElem
            |> Maybe.map
                (\m ->
                    [ getThingNameFromId inbox.users m.sender_id, ": ", m.body ]
                        |> String.concat
                        |> String.left 100
                )
            |> Maybe.withDefault ""
            |> text
        ]


renderThreadChannelTag : Model -> Warp -> Thread -> Html Msg
renderThreadChannelTag model inbox thread =
    case model.current_channel_id of
        Just int ->
            span [] []

        Nothing ->
            span [ class "level-item", rel "noreferrer" ]
                [ span [ class "tag channel-tag is-info fs85" ]
                    [ "#"
                        ++ getThingNameFromId inbox.channels thread.channel_id
                        |> text
                    ]
                ]



--
-- renderJoinChannelsModal : Model -> Html Msg
-- renderJoinChannelsModal model =
--     div
--         [ class
--             (case model.isJoinChannelsModalActive of
--                 True ->
--                     "modal is-active"
--
--                 False ->
--                     "modal"
--             )
--         ]
--         [ div [ class "modal-background" ]
--             []
--         , div [ class "modal-content" ]
--             [ header [ class "modal-card-head" ]
--                 [ p [ class "modal-card-title" ]
--                     [ text "Create channels for your team" ]
--                 ]
--             , section [ class "modal-card-body" ]
--                 [ div [ class "field " ]
--                     [ label [ class "label" ]
--                         [ text "Your Team Name" ]
--                     , div [ class "field " ]
--                         [ div [ class "control" ]
--                             [ input
--                                 [ class "input"
--                                 , placeholder "e.g. Lakme"
--                                 , type_ "text"
--                                 , onInput NewTeamNameChanged
--                                 , Html.Attributes.value model.newTeamName
--                                 ]
--                                 []
--                             ]
--                         ]
--                     ]
--                 ]
--             , footer [ class "modal-card-foot" ]
--                 [ button [ class "button is-success", onClick CreateNewChannels ]
--                     [ text "Create Channels" ]
--                 ]
--             ]
--         ]
