module Page.User.Profile exposing (Msg(..), subscriptions, update, view)

import Api.Profile as Profile
import Html exposing (..)
import Html.Attributes exposing (..)
import Types exposing (..)
import Utils exposing (..)


type Msg
    = NoOp


subscriptions : Profile.Model -> Sub Msg
subscriptions model =
    Sub.none


update : Context -> Msg -> Profile.Model -> ( Profile.Model, Cmd Msg, Context )
update ctx msg model =
    ( model, Cmd.none, ctx )


view : Profile.Model -> Html Msg
view model =
    section [ class "section" ]
        [ div [ class "container" ]
            [ h1 [ class "title" ] [ text model.name ]
            , h2 [ class "subtitle" ] [ text model.email ]
            , p [] []
            , div [ class "content" ]
                [ h2 [ class "title" ] [ text "Your teams:" ]
                , ul []
                    (List.map
                        (\team ->
                            li []
                                [ a [ href ("/#/teams/" ++ String.fromInt team.id) ] [ text team.name ]
                                ]
                        )
                        model.teams
                    )
                ]
            ]
        ]
