module Page.User.Login exposing (Msg(..), authUser, ping, subscriptions, update, view)

-- import Navigation exposing (modifyUrl, pushUrl)

import Api.User as User
import Browser.Navigation as Navigation
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Json.Encode as JE exposing (..)
import Ports exposing (..)
import RemoteData exposing (..)
import RemoteData.Http
import Types exposing (..)
import Utils exposing (..)



-- model


type Msg
    = LoginResponse (WebData User.Token)
    | LoginClicked
    | UpdateEmail String
    | UpdatePassword String
    | LogoutClicked
      -- | LoadToken (Maybe User.Token)
    | NoOp (WebData User.Ping)


subscriptions : User.Model -> Sub Msg
subscriptions model =
    Sub.none



-- update


update : Context -> Msg -> User.Model -> ( User.Model, Cmd Msg, Context )
update ctx msg model =
    let
        noChange =
            ( model, Cmd.none )
    in
    case msg of
        LoginClicked ->
            ( model, authUser ctx model, ctx )

        LoginResponse (Success result) ->
            let
                newCtx_ =
                    { ctx | token = Just result, errorMsg = Nothing }

                newUrl =
                    result.teams
                        |> List.head
                        |> Maybe.map (\team -> "/#/teams/" ++ String.fromInt team.id)
                        |> Maybe.withDefault "/#/onboarding/"
            in
            ( model
              -- User.WebDatax result
            , Cmd.batch
                [ User.encodeToken result
                    |> JE.encode 0
                    |> Just
                    |> setToken
                , Navigation.pushUrl ctx.key newUrl
                ]
            , newCtx_
            )

        LoginResponse (Failure err) ->
            let
                error =
                    httpErrorString err

                newCtx_ =
                    { ctx | errorMsg = Just error }
            in
            ( model, Cmd.none, newCtx_ )

        UpdateEmail email ->
            case model of
                User.UserAuthData userAuth ->
                    ( User.UserAuthData { userAuth | email = email }, Cmd.none, ctx )

                User.WebDatax token ->
                    ( model, Cmd.none, ctx )

        UpdatePassword pwd ->
            case model of
                User.UserAuthData userAuth ->
                    ( User.UserAuthData { userAuth | password = pwd }, Cmd.none, ctx )

                User.WebDatax token ->
                    ( model, Cmd.none, ctx )

        LogoutClicked ->
            ( User.initialModel
            , Cmd.batch
                [ setToken Nothing
                , Navigation.pushUrl ctx.key "/#/login"
                ]
            , resetContext ctx
            )

        --
        -- LoadToken token ->
        --     case token of
        --         Just t ->
        --             let
        --                 newCtx =
        --                     { ctx | token = token, errorMsg = Nothing }
        --             in
        --             ( ( User.WebDatax t
        --               , Cmd.batch
        --                     [ Navigation.pushUrl ctx.key "/#/inbox"
        --                     ]
        --               )
        --             , newCtx
        --             )
        --
        --         Nothing ->
        --             ( ( model, Navigation.pushUrl ctx.key "/#/login" ), ctx )
        NoOp _ ->
            ( model, Cmd.none, ctx )

        _ ->
            ( model, Cmd.none, ctx )



-- http requests
-- POST register / login request


authUser : Context -> User.Model -> Cmd Msg
authUser ctx model =
    case Debug.log "authUser" model of
        User.UserAuthData userAuth ->
            RemoteData.Http.post
                (ctx.apiRoot ++ "/sessions")
                LoginResponse
                User.decodeToken
                (User.encodeUserAuth userAuth)

        User.WebDatax token ->
            Cmd.none


ping : Context -> Cmd Msg
ping ctx =
    RemoteData.Http.get
        (ctx.apiRoot ++ "/sessions/ping")
        NoOp
        User.decodePing



-- view


view : User.Model -> Html Msg
view model =
    case model of
        User.UserAuthData userAuth ->
            section [ class "hero is-success is-fullheight" ]
                [ div [ class "hero-body" ]
                    [ div [ class "container has-text-centered" ]
                        [ div [ class "column is-4 is-offset-4" ]
                            [ h3 [ class "title has-text-grey" ]
                                [ text "Login" ]
                            , p [ class "subtitle has-text-grey" ]
                                [ text "Please login to proceed." ]
                            , div [ class "box" ]
                                [ figure [ class "avatar" ]
                                    [ img [ src "/logo.png" ]
                                        []
                                    ]
                                , Html.form []
                                    [ div [ class "field" ]
                                        [ div [ class "control" ]
                                            [ input
                                                [ attribute "autofocus" ""
                                                , class "input is-large"
                                                , placeholder "Your Email"
                                                , type_ "email"
                                                , onInput UpdateEmail
                                                , value userAuth.email
                                                ]
                                                []
                                            ]
                                        ]
                                    , div [ class "field" ]
                                        [ div [ class "control" ]
                                            [ input
                                                [ class "input is-large"
                                                , placeholder "Your Password"
                                                , type_ "password"
                                                , onInput UpdatePassword
                                                , value userAuth.password
                                                ]
                                                []
                                            ]
                                        ]
                                    , a
                                        [ onClick LoginClicked
                                        , class "button is-block is-info is-large"
                                        , href "#"
                                        ]
                                        [ text "Login" ]
                                    ]
                                ]
                            , p [ class "has-text-grey" ]
                                [ a [ href "/#signup" ]
                                    [ text "Sign Up" ]

                                -- , text " ·             "
                                -- , a [ href "/" ]
                                --     [ text "Forgot Password" ]
                                ]
                            ]
                        ]
                    ]
                ]

        User.WebDatax token ->
            h1 [] [ text "Already logged in" ]
