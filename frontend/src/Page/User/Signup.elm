module Page.User.Signup exposing (Msg(..), signupUser, subscriptions, update, view)

import Api.Signup as Signup
import Api.User as User
import Browser.Navigation as Navigation
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Json.Encode as JE exposing (..)
import Ports exposing (..)
import RemoteData exposing (RemoteData(..), WebData)
import RemoteData.Http
import Types exposing (..)
import Utils exposing (..)



-- model


type Msg
    = OnSignupResp (WebData User.Token)
    | SignupClicked
    | UpdateEmail String
    | UpdatePassword String
    | UpdateName String
    | LogoutClicked
    | LoadToken (Maybe User.Token)


subscriptions : Signup.Model -> Sub Msg
subscriptions model =
    Sub.none



-- update


update : Context -> Msg -> Signup.Model -> ( Signup.Model, Cmd Msg, Context )
update ctx msg model =
    case msg of
        SignupClicked ->
            ( model, signupUser ctx model, ctx )

        OnSignupResp (Success result) ->
            let
                newCtx =
                    { ctx | token = Just result, errorMsg = Nothing }
            in
            ( Signup.WebDatax result
            , Cmd.batch
                [ result
                    |> User.encodeToken
                    |> JE.encode 0
                    |> Just
                    |> setToken
                , Navigation.pushUrl ctx.key "/#/newteam"
                ]
            , newCtx
            )

        OnSignupResp (Failure err) ->
            let
                error =
                    httpErrorString err

                newCtx =
                    { ctx | errorMsg = Just error }
            in
            ( model, Cmd.none, newCtx )

        OnSignupResp _ ->
            ( model, Cmd.none, ctx )

        UpdateEmail email ->
            case model of
                Signup.UserAuthData userAuth ->
                    ( Signup.UserAuthData { userAuth | email = email }, Cmd.none, ctx )

                Signup.WebDatax token ->
                    ( model, Cmd.none, ctx )

        UpdatePassword pwd ->
            case model of
                Signup.UserAuthData userAuth ->
                    ( Signup.UserAuthData { userAuth | password = pwd }, Cmd.none, ctx )

                Signup.WebDatax token ->
                    ( model, Cmd.none, ctx )

        UpdateName name ->
            case model of
                Signup.UserAuthData userAuth ->
                    ( Signup.UserAuthData { userAuth | name = name }, Cmd.none, ctx )

                Signup.WebDatax token ->
                    ( model, Cmd.none, ctx )

        LogoutClicked ->
            ( Signup.initialModel
            , Cmd.batch
                [ setToken Nothing
                , Navigation.pushUrl ctx.key "/#/login"
                ]
            , resetContext ctx
            )

        LoadToken token ->
            case token of
                Just t ->
                    let
                        newCtx =
                            { ctx | token = token, errorMsg = Nothing }
                    in
                    ( Signup.WebDatax t
                    , Cmd.batch
                        [ Navigation.pushUrl ctx.key "/#/inbox"
                        ]
                    , newCtx
                    )

                Nothing ->
                    ( model, Navigation.pushUrl ctx.key "/#/login", ctx )



-- http requests
-- POST register / login request


signupUser : Context -> Signup.Model -> Cmd Msg
signupUser ctx model =
    case model of
        Signup.UserAuthData userAuth ->
            RemoteData.Http.post
                (ctx.apiRoot ++ "/users")
                OnSignupResp
                User.decodeToken
                (Signup.encodeUserAuth userAuth)

        Signup.WebDatax token ->
            Cmd.none



-- view


view : Signup.Model -> Html Msg
view model =
    case model of
        Signup.UserAuthData userAuth ->
            section [ class "hero is-success is-fullheight" ]
                [ div [ class "hero-body" ]
                    [ div [ class "container has-text-centered" ]
                        [ div [ class "column is-4 is-offset-4" ]
                            [ h3 [ class "title has-text-grey" ]
                                [ text "Sign up" ]
                            , p [ class "subtitle has-text-grey" ]
                                [ text "Please sign up to proceed." ]
                            , div [ class "box" ]
                                [ figure [ class "avatar" ]
                                    [ img [ src "/logo.png" ]
                                        []
                                    ]
                                , Html.form []
                                    [ div [ class "field" ]
                                        [ div [ class "control" ]
                                            [ input
                                                [ attribute "autofocus" ""
                                                , class "input is-large"
                                                , placeholder "Your Email"
                                                , type_ "email"
                                                , onInput UpdateEmail
                                                , value userAuth.email
                                                ]
                                                []
                                            ]
                                        ]
                                    , div [ class "field" ]
                                        [ div [ class "control" ]
                                            [ input
                                                [ attribute "autofocus" ""
                                                , class "input is-large"
                                                , placeholder "Your Name"
                                                , type_ "text"
                                                , onInput UpdateName
                                                , value userAuth.name
                                                ]
                                                []
                                            ]
                                        ]
                                    , div [ class "field" ]
                                        [ div [ class "control" ]
                                            [ input
                                                [ class "input is-large"
                                                , placeholder "Your Password"
                                                , type_ "password"
                                                , onInput UpdatePassword
                                                , value userAuth.password
                                                ]
                                                []
                                            ]
                                        ]
                                    , a
                                        [ onClick SignupClicked
                                        , class "button is-block is-info is-large"
                                        ]
                                        [ text "Signup" ]
                                    ]
                                ]
                            , p [ class "has-text-grey" ]
                                [ a [ href "/#/login" ]
                                    [ text "Login" ]

                                -- , text " ·             "
                                -- , a [ href "/" ]
                                --     [ text "Forgot Password" ]
                                ]
                            ]
                        ]
                    ]
                ]

        Signup.WebDatax token ->
            h1 [] [ text "Already logged in" ]
