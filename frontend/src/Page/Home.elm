module Page.Home exposing (Model, Msg(..), init, update, view)

import Browser.Navigation as Navigation
import Html exposing (..)
import Types exposing (..)


type alias Model =
    String


type Msg
    = NoOp


init : Context -> ( Model, Cmd Msg )
init ctx =
    let
        newUrl =
            case ctx.token of
                Just token ->
                    token.teams
                        |> List.head
                        |> Maybe.map (\team -> "/#/teams/" ++ String.fromInt team.id)
                        |> Maybe.withDefault "/#/onboarding/"

                Nothing ->
                    "/#/onboarding/"

        _ =
            Debug.log "newUrl" newUrl
    in
    ( ""
    , Navigation.pushUrl ctx.key newUrl
    )


update : Context -> Msg -> Model -> ( Model, Cmd Msg, Context )
update ctx msg model =
    ( model, Cmd.none, ctx )


view : Model -> Html Msg
view model =
    h1 [] [ text "Welcome to Warp!" ]
