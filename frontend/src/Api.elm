module Api exposing
    ( FormErrors
    , addError
    , delete
    , errorHtml
    , formErrorsDecoder
    , get
    , getErrorHtml
    , initialErrors
    , post
    , put
    , sendRequest
    , toRequest
    , toTask
    , withErrorHandler
    , withJsonBody
    , withJsonResponse
    , withToken
    )

-- import Products.Pagination as Pagination
-- import Routing.Utils exposing (joinPath, queryParameter, withQueryStrings)

import Dict exposing (Dict)
import Html
import Html.Attributes exposing (class)
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Encode exposing (Value)
import RemoteData
import Task exposing (Task)
import Time
import Utils



-- API ENDPOINTS
-- | CategoryDetails String Pagination.Data
-- | CustomerMyAccount (Maybe Int)
-- authUser : Model -> Cmd Msg
-- authUser model =
--     case model of
--         UserAuthData userAuth ->
--             RemoteData.Http.post
--                 (apiRoot ++ "/sessions")
--                 OnAuthUserResp
--                 decodeToken
--                 (encodeUserAuth userAuth)
--
--         WebData token ->
--             Cmd.none
--
-- toUrl : Endpoint -> String
-- toUrl endpoint =
--     let
--         endpointUrl =
--             case endpoint of
--                 AuthUser ->
--                     joinPath [ "sessions" ]
--
--         -- CategoryDetails slug data ->
--         --     joinPath [ "categories", "details", slug ]
--         --         ++ withQueryStrings [ Pagination.toQueryString data ]
--         --
--         -- CustomerMyAccount (Just limit) ->
--         --     joinPath [ "customers", "my-account" ]
--         --         ++ withQueryStrings
--         --             [ queryParameter ( "limit", toString limit ) ]
--     in
--     "/api" ++ endpointUrl
-- REQUEST BUILDING


type alias CustomRequest a =
    { method : String
    , headers : List Http.Header
    , url : String
    , body : Http.Body
    , expect : Http.Expect a

    -- , timeout : Maybe Time.Time
    , timeout : Maybe Float
    , withCredentials : Bool
    }



-- Todo : Url


type alias Endpoint =
    String


initialRequest : CustomRequest String
initialRequest =
    { method = "GET"
    , headers = []
    , url = ""
    , body = Http.emptyBody
    , expect = Http.expectString
    , timeout = Nothing
    , withCredentials = False
    }


initialMethod : String -> Endpoint -> CustomRequest String
initialMethod method endpoint =
    { initialRequest | method = method, url = Utils.apiRoot ++ endpoint }


get : Endpoint -> CustomRequest String
get =
    initialMethod "GET"


post : Endpoint -> CustomRequest String
post =
    initialMethod "POST"


put : Endpoint -> CustomRequest String
put =
    initialMethod "PUT"


delete : Endpoint -> CustomRequest String
delete =
    initialMethod "DELETE"


withToken : String -> CustomRequest a -> CustomRequest a
withToken token request =
    { request | headers = Http.header "Authorization" ("Bearer " ++ token) :: request.headers }



-- Http.header "Authorization" ("Bearer " ++ token)


withJsonBody : Value -> CustomRequest a -> CustomRequest a
withJsonBody body request =
    { request | body = Http.jsonBody body }


withJsonResponse : Decoder a -> CustomRequest b -> CustomRequest a
withJsonResponse decoder request =
    -- { request | expect = Http.expectJson decoder }
    { method = request.method
    , headers = request.headers
    , url = request.url
    , body = request.body
    , timeout = request.timeout
    , withCredentials = request.withCredentials

    -- changed
    , expect = Http.expectJson decoder
    }


withErrorHandler : (RemoteData.WebData (Result Http.Error a) -> msg) -> CustomRequest a -> Cmd msg
withErrorHandler msg request =
    let
        errorHandler response =
            case response of
                -- RemoteData.Failure (Http.BadStatus rawResponse) ->
                --     if rawResponse.status.code == validationErrorCode then
                --         case Decode.decodeString formErrorsDecoder rawResponse.body of
                --             Ok errors ->
                --                 RemoteData.Success <| Err errors
                --
                --             Err _ ->
                --                 RemoteData.Failure <| Http.BadStatus rawResponse
                --
                --     else
                --         RemoteData.Failure <| Http.BadStatus rawResponse
                RemoteData.Failure error ->
                    RemoteData.Failure error

                RemoteData.Success data ->
                    RemoteData.Success <| Ok data

                RemoteData.Loading ->
                    RemoteData.Loading

                RemoteData.NotAsked ->
                    RemoteData.NotAsked
    in
    request
        |> Http.request
        |> RemoteData.sendRequest
        |> Cmd.map (errorHandler >> msg)


sendRequest : (RemoteData.WebData a -> msg) -> CustomRequest a -> Cmd msg
sendRequest msg =
    Http.request >> RemoteData.sendRequest >> Cmd.map msg


toRequest : CustomRequest a -> Http.Request a
toRequest =
    Http.request


toTask : CustomRequest a -> Task Http.Error a
toTask =
    toRequest >> Http.toTask



-- API VALIDATION ERROR RESPONSES


validationErrorCode : Int
validationErrorCode =
    422


type alias Field =
    String


type alias ErrorMessage =
    String


type alias FormErrors =
    Dict Field (List ErrorMessage)


initialErrors : FormErrors
initialErrors =
    Dict.empty


formErrorsDecoder : Decoder FormErrors
formErrorsDecoder =
    Decode.dict <| Decode.list Decode.string


addError : Field -> ErrorMessage -> FormErrors -> FormErrors
addError field message errors =
    Utils.flip (Dict.update field) errors <|
        \val ->
            case val of
                Nothing ->
                    Just [ message ]

                Just es ->
                    Just <| message :: es


{-| Render a field's error messages in red text.
-}
getErrorHtml : Field -> FormErrors -> Html.Html msg
getErrorHtml field errors =
    Dict.get field errors
        |> Maybe.map errorHtml
        |> Maybe.withDefault (Html.text "")


{-| Render a list of error messages in red text.
-}
errorHtml : List ErrorMessage -> Html.Html msg
errorHtml =
    List.map Html.text
        >> List.intersperse (Html.br [] [])
        >> Html.div [ class "text-danger" ]
