module Model exposing (Model, Page(..))

import Api.Profile as Profile
import Api.Signup as Signup
import Api.Team as Team
import Api.User as User
import Page.Home as HomePage
import Page.Inbox.CreateChannel as CreateChannelPage
import Page.Inbox.Onboarding as OnboardingPage
import Page.Inbox.Team as TeamPage
import Page.NotFound
import Page.User.Login as LoginPage
import Page.User.Profile as ProfilePage
import Page.User.Signup as SignupPage
import Routing exposing (..)
import Types exposing (..)


type Page
    = TeamPage Team.Model
    | LoginPage User.Model
    | SignupPage Signup.Model
    | HomePage HomePage.Model
    | OnboardingPage OnboardingPage.Model
    | CreateChannelPage CreateChannelPage.Model
    | ProfilePage Profile.Model
    | NotFoundPage


type alias Model =
    { pageState : Page
    , route : Route
    , ctx : Context

    -- , phxSocket : Phoenix.Socket.Socket Msg
    }
