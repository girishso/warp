module Routing exposing (Route(..), matchers, parseFragmentAsPath, parseUrlFragmentAsPath, routeRequiresAuth)

import Types exposing (..)
import Url exposing (Url)
import Url.Parser exposing ((</>), Parser, int, s, top)


type Route
    = TeamRoute String
    | LoginRoute
    | SignupRoute
    | HomeRoute
    | NotFoundRoute
      -- | InboxRoute
    | ProfileRoute
    | OnboardingRoute
    | CreateChannelRoute


routeRequiresAuth : Route -> Bool
routeRequiresAuth route =
    case route of
        TeamRoute _ ->
            True

        LoginRoute ->
            False

        HomeRoute ->
            True

        -- InboxRoute ->
        --     True
        SignupRoute ->
            False

        ProfileRoute ->
            True

        NotFoundRoute ->
            False

        OnboardingRoute ->
            True

        CreateChannelRoute ->
            True


matchers : Parser (Route -> a) a
matchers =
    Url.Parser.oneOf
        [ Url.Parser.map LoginRoute (Url.Parser.s "login")
        , Url.Parser.map SignupRoute (Url.Parser.s "signup")
        , Url.Parser.map TeamRoute (Url.Parser.s "teams" </> Url.Parser.string)

        -- , Url.Parser.map InboxRoute (Url.Parser.s "inbox")
        , Url.Parser.map ProfileRoute (Url.Parser.s "profile")
        , Url.Parser.map OnboardingRoute (Url.Parser.s "onboarding")
        , Url.Parser.map CreateChannelRoute (Url.Parser.s "createchannel")
        , Url.Parser.map HomeRoute top
        ]



--parseLocation : Url -> Route
--parseLocation url =
--    case parseHash matchers url of
--        Just route ->
--            route
--
--        Nothing ->
--            NotFoundRoute


parseUrlFragmentAsPath : Url.Url -> Route
parseUrlFragmentAsPath url =
    { url | path = Maybe.withDefault "" url.fragment, fragment = Nothing }
        |> Url.Parser.parse matchers
        |> Maybe.withDefault NotFoundRoute


parseFragmentAsPath : String -> Route
parseFragmentAsPath path =
    let
        url =
            { protocol = Url.Https
            , host = ""
            , port_ = Nothing
            , path = path
            , query = Nothing
            , fragment = Nothing
            }
    in
    Url.Parser.parse matchers url |> Maybe.withDefault NotFoundRoute
