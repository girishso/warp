port module Ports exposing (loadedToken, scrollTo, setHideSwitchTeamsTT, setToken)

import Api.User as User
import Types exposing (..)


port scrollTo : String -> Cmd msg


port setToken : Maybe String -> Cmd msg


port setHideSwitchTeamsTT : () -> Cmd msg


port loadedToken : (Maybe User.Token -> msg) -> Sub msg



-- port startLoadToken : () -> Cmd msg
