module Utils exposing (Error, apiRoot, appendErrors, debugResponse, decodeAlways, decodeError, flip, getFailureErrorStr, getRandomColor, httpConfig, httpErrorString, isBlank, isJust, isNothing, isPresent, lastElem, showErrorIfFailed, showErrorIfFailed2, stringAsciiCodeSum, viewIf, webDataToMaybe, webSocket_Url)

-- import Html.Events exposing (defaultOptions, onWithOptions)

import Array exposing (fromList)
import Char exposing (toCode)
import Html exposing (Attribute, Html)
import Http exposing (..)
import Json.Decode as JD exposing (field)
import RemoteData exposing (..)
import RemoteData.Http


flip f a b =
    f b a


httpConfig : String -> RemoteData.Http.Config
httpConfig token =
    { headers =
        [ Http.header "Authorization" ("Bearer " ++ token)
        ]
    , withCredentials = False
    , timeout = Nothing
    }



-- (=>) : a -> b -> ( a, b )
-- (=>) =
--     \a b -> ( a, b )
--
--
-- {-| infixl 0 means the (=>) operator has the same precedence as (<|) and (|>),
-- meaning you can use it at the end of a pipeline and have the precedence work out.
-- -}
--
--
-- infixl 0 =>
--


{-| Useful when building up a Cmd via a pipeline, and then pairing it with
a model at the end.

    session.user
        |> User.Request.foo
        |> Task.attempt Foo
        |> pair { model | something = blah }

-}



-- pair : a -> b -> ( a, b )
-- pair first second =
--     first => second


showErrorIfFailed data model ctx successResponse =
    if isFailure data then
        let
            errStr =
                case data of
                    Failure e ->
                        httpErrorString e

                    _ ->
                        ""
        in
        ( model, Cmd.none, { ctx | errorMsg = Just errStr } )

    else
        successResponse


showErrorIfFailed2 response model ctx successResponse =
    case response of
        RemoteData.Success (Ok data) ->
            successResponse data

        RemoteData.Success (Err errors) ->
            -- let
            --     errStr =
            --         httpErrorString errors
            -- in
            -- -- ( { model | errors = errors }, Nothing, Ports.scrollToTop )
            ( model, Cmd.none, { ctx | errorMsg = Just (httpErrorString errors) } )

        _ ->
            debugResponse response model ctx


debugResponse response model ctx =
    let
        _ =
            Debug.log "Bad Response" response
    in
    ( model, Cmd.none, ctx )


viewIf : Bool -> Html msg -> Html msg
viewIf condition content =
    if condition then
        content

    else
        Html.text ""



-- onClickStopPropagation : msg -> Attribute msg
-- onClickStopPropagation msg =
--     onWithOptions "click"
--         { defaultOptions | stopPropagation = True }
--         (JD.succeed msg)


appendErrors : { model | errors : List error } -> List error -> { model | errors : List error }
appendErrors model errors =
    { model | errors = model.errors ++ errors }


lastElem : List a -> Maybe a
lastElem =
    List.foldl (Just >> always) Nothing


apiRoot : String
apiRoot =
    "http://localhost:4000/api"



-- "https://serene-shore-69882.herokuapp.com/api"


webDataToMaybe : WebData a -> Maybe a
webDataToMaybe data =
    case data of
        Success d ->
            Just d

        _ ->
            Nothing


webSocket_Url : String
webSocket_Url =
    "ws://localhost:8000/socket/websocket?token="



-- "wss://serene-shore-69882.herokuapp.com/socket/websocket?token="


type alias Error =
    { error : String
    }


getFailureErrorStr : Http.Error -> String
getFailureErrorStr err =
    case err of
        BadUrl string ->
            string

        Timeout ->
            "Network time out error"

        NetworkError ->
            "NetworkError"

        BadPayload string stringResponseHttp ->
            "Response error"

        Http.BadStatus res ->
            case JD.decodeString decodeError res.body of
                Ok v ->
                    v.error

                Err e ->
                    -- maybe errors exist
                    "An error occured: " ++ Debug.toString e


httpErrorString : Http.Error -> String
httpErrorString error =
    case error of
        BadUrl text ->
            "Bad Url: " ++ text

        Timeout ->
            "Http Timeout"

        NetworkError ->
            "Network Error"

        BadStatus response ->
            case JD.decodeString decodeError response.body of
                Ok v ->
                    v.error

                Err e ->
                    -- maybe errors exist
                    "An error occured: " ++ Debug.toString e

        BadPayload message response ->
            "Bad Http Payload: "
                ++ Debug.toString message
                ++ " ("
                ++ Debug.toString response.status.code
                ++ ")"


decodeAlways : JD.Decoder String
decodeAlways =
    JD.andThen (\_ -> JD.succeed "") JD.value


decodeError : JD.Decoder Error
decodeError =
    JD.map Error
        (field "error" JD.string)


stringAsciiCodeSum : String -> Int
stringAsciiCodeSum str =
    String.toList str
        |> List.map toCode
        |> List.foldr (+) 0


getRandomColor : String -> String
getRandomColor str =
    let
        defaultColors =
            Array.fromList
                [ "#d73d32"
                , "#d61a7f"
                , "#ff4080"
                , "#d35400"
                , "#f1c40f"
                , "#7e3794"
                , "#4285f4"
                , "#67ae3f"
                ]

        sum =
            stringAsciiCodeSum str

        idx =
            modBy (Array.length defaultColors) sum
    in
    Maybe.withDefault "#ddd" (Array.get idx defaultColors)


{-| Conveniently check if a `Maybe` matches `Nothing`.
isNothing (Just 42) == False
isNothing (Just []) == False
isNothing Nothing == True
-}
isNothing : Maybe a -> Bool
isNothing m =
    case m of
        Nothing ->
            True

        Just _ ->
            False


{-| Conveniently check if a `Maybe` matches `Just _`.
isJust (Just 42) == True
isJust (Just []) == True
isJust Nothing == False
-}
isJust : Maybe a -> Bool
isJust m =
    case m of
        Nothing ->
            False

        Just _ ->
            True


isPresent : String -> Bool
isPresent =
    isBlank >> not


isBlank : String -> Bool
isBlank =
    String.trim >> String.isEmpty
