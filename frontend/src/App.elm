port module App exposing (main, subscriptions)

-- import Phoenix.Socket

import Browser
import Browser.Navigation as Navigation
import Json.Decode as JD
import Json.Encode as JE
import Main exposing (..)
import Model exposing (Model)
import Page.User.Login as LoginPage
import Ports exposing (..)
import Time
import Types exposing (..)
import Url exposing (Url)
import Utils exposing (..)


subscriptions : Model -> Sub Msg
subscriptions model =
    let
        tickerMsg =
            model.ctx.errorMsg
                |> Maybe.map (\_ -> Time.every 3000 (always HideToast))
                |> Maybe.withDefault Sub.none
    in
    Sub.batch
        [ tickerMsg

        -- , Sub.map LoginMsg (Ports.loadedToken LoginPage.LoadToken)
        , Sub.none

        --        , Phoenix.Socket.listen model.phxSocket PhoenixMsg
        ]



---- PROGRAM ----


main : Program JD.Value Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        }
