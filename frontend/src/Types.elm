module Types exposing (Context, Flags, Team, initContext, resetContext)

import Api.User as User
import Browser.Navigation as Navigation
import Url exposing (Url)


type alias Context =
    { errorMsg : Maybe String
    , token : Maybe User.Token
    , teams : List Team
    , currentTeam : Maybe Team
    , showSwitchTeamsTooltip : Bool
    , key : Navigation.Key
    , url : Url
    , env : User.Env
    , apiRoot : String
    }


type alias Team =
    { name : String
    , id : Int
    , descr : String
    }


type alias Flags =
    { showSwitchTeamsTooltip : Bool
    , token : Maybe User.Token
    , env : User.Env
    }


initContext : Navigation.Key -> Url -> Maybe User.Token -> Bool -> User.Env -> Context
initContext key url token showSwitchTeamsTooltip env =
    { errorMsg = Nothing
    , token = token
    , teams = []
    , currentTeam = Nothing
    , showSwitchTeamsTooltip = showSwitchTeamsTooltip
    , url = url
    , key = key
    , env = env
    , apiRoot =
        if env == User.Development then
            "http://localhost:4000/api"

        else
            "https://serene-shore-69882.herokuapp.com/api"
    }


resetContext : Context -> Context
resetContext ctx =
    { ctx
        | errorMsg = Nothing
        , token = Nothing
        , teams = []
        , currentTeam = Nothing
        , showSwitchTeamsTooltip = False
    }
