module Routing.Utils exposing
    ( fromIntParam
    , joinPath
    , joinQueryStrings
    , optionalIntParam
    , parseFlag
    , queryFlag
    , queryParameter
    , withQueryStrings
    )

-- import UrlParser exposing (Parser, UP.custom)

import Url.Parser as UP exposing (..)



-- import Url.Parser.Query exposing (..)
-- Parsing


fromIntParam : String -> (Int -> a) -> Parser (Maybe a -> b) b
fromIntParam param fromInt =
    UP.custom param String.toInt



-- (Maybe.andThen String.toInt
--     >> Maybe.map fromInt
-- )


optionalIntParam : String -> Int -> Parser (Int -> b) b
optionalIntParam param default =
    UP.custom param
        (Maybe.andThen (String.toInt >> Result.toMaybe)
            >> Maybe.withDefault default
        )


parseFlag : String -> Parser (Bool -> b) b
parseFlag param =
    UP.custom param (Maybe.map ((==) "1") >> Maybe.withDefault False)



-- Building


joinPath : List String -> String
joinPath paths =
    String.join "/" <| "" :: paths ++ [ "" ]


queryParameter : ( String, String ) -> String
queryParameter ( name, value ) =
    name ++ "=" ++ value


queryFlag : String -> Bool -> String
queryFlag flag showFlag =
    if showFlag then
        queryParameter ( flag, "1" )

    else
        ""


withQueryStrings : List String -> String
withQueryStrings =
    joinQueryStrings >> prependQueryStart


joinQueryStrings : List String -> String
joinQueryStrings =
    List.filter (not << String.isEmpty)
        >> String.join "&"


prependQueryStart : String -> String
prependQueryStart queryString =
    if String.isEmpty queryString then
        ""

    else
        "?" ++ queryString
