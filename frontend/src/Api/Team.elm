module Api.Team exposing (Channel, Message, Model, Thread, ThreadNMessage, User, Warp, decodeChannel, decodeChannels, decodeMessage, decodeTeam, decodeTeams, decodeThread, decodeThreadNMessage, decodeUser, decodeWarp, encodeChannel, encodeMessage, encodeTeam, encodeThread, encodeThreadNMessage, initialModel)

import Json.Decode as JD
import Json.Decode.Pipeline as Pipeline exposing (optional, required)
import Json.Encode as JE exposing (..)
import Types exposing (..)



-- Model
-- Types


type alias Model =
    { inbox : Maybe Warp

    --
    , current_thread_id : Int
    , current_channel_id : Maybe Int

    --
    , newMessageBody : String

    --
    , isNewChannelModelActive : Bool
    , newChannelName : String

    --
    , isNewTeamModalActive : Bool
    , newTeamName : String

    --
    , isStartNewThreadActive : Bool
    , newThreadSubject : String

    --
    , isJoinChannelsModalActive : Bool
    }


type alias Warp =
    { users : List User
    , team : Team
    , messages : List Message
    , threads : List Thread
    , channels : List Channel
    }


type alias User =
    { name : String
    , id : Int
    , email : String
    }


type alias Channel =
    { name : String
    , id : Int
    , descr : String
    }


type alias Thread =
    { subject : String
    , id : Int
    , channel_id : Int
    , team_id : Int
    , inserted_at : String
    , user_id : Int
    , un_read : Bool
    }


type alias Message =
    { thread_id : Int
    , team_id : Int
    , sender_id : Int
    , recipient_ids : List Int
    , id : Int
    , channel_id : Int
    , body : String
    , inserted_at : String
    , inserted_ago : String
    }


type alias ThreadNMessage =
    { thread : Thread, message : Message }


initialModel : Model
initialModel =
    { inbox = Nothing
    , current_thread_id = 0
    , current_channel_id = Nothing
    , newMessageBody = ""
    , isNewChannelModelActive = False
    , isNewTeamModalActive = False
    , isStartNewThreadActive = False
    , isJoinChannelsModalActive = False
    , newChannelName = ""
    , newTeamName = ""
    , newThreadSubject = ""
    }



-- json decoders


decodeWarp : JD.Decoder Warp
decodeWarp =
    JD.succeed Warp
        |> Pipeline.required "users" (JD.list decodeUser)
        |> Pipeline.required "team" decodeTeam
        |> Pipeline.required "messages" (JD.list decodeMessage)
        |> Pipeline.required "threads" (JD.list decodeThread)
        |> Pipeline.required "channels" (JD.list decodeChannel)


decodeThreadNMessage : JD.Decoder ThreadNMessage
decodeThreadNMessage =
    JD.map2 ThreadNMessage
        (JD.field "thread" decodeThread)
        (JD.field "message" decodeMessage)


decodeTeams : JD.Decoder (List Team)
decodeTeams =
    JD.field "teams" (JD.list decodeTeam)


decodeChannels : JD.Decoder (List Channel)
decodeChannels =
    JD.field "channels" (JD.list decodeChannel)



-- decodeTeams : JD.Decoder Teams
-- decodeTeams =
--     JD.succeed Teams
--         |> Pipeline.required "teams" (JD.list decodeTeam)


decodeUser : JD.Decoder User
decodeUser =
    JD.succeed User
        |> Pipeline.required "name" JD.string
        |> Pipeline.required "id" JD.int
        |> Pipeline.required "email" JD.string


decodeThread : JD.Decoder Thread
decodeThread =
    JD.succeed Thread
        |> Pipeline.required "subject" JD.string
        |> Pipeline.required "id" JD.int
        |> Pipeline.required "channel_id" JD.int
        |> Pipeline.required "team_id" JD.int
        |> Pipeline.required "inserted_at" JD.string
        |> Pipeline.required "user_id" JD.int
        |> Pipeline.required "un_read" JD.bool


decodeChannel : JD.Decoder Channel
decodeChannel =
    JD.succeed Channel
        |> Pipeline.required "name" JD.string
        |> Pipeline.required "id" JD.int
        |> Pipeline.optional "descr" JD.string ""


decodeTeam : JD.Decoder Team
decodeTeam =
    JD.succeed Team
        |> Pipeline.required "name" JD.string
        |> Pipeline.required "id" JD.int
        |> Pipeline.optional "descr" JD.string ""


decodeMessage : JD.Decoder Message
decodeMessage =
    JD.succeed Message
        |> Pipeline.required "thread_id" JD.int
        |> Pipeline.required "team_id" JD.int
        |> Pipeline.required "sender_id" JD.int
        |> Pipeline.required "recipient_ids" (JD.list JD.int)
        |> Pipeline.required "id" JD.int
        |> Pipeline.required "channel_id" JD.int
        |> Pipeline.required "body" JD.string
        |> Pipeline.required "inserted_at" JD.string
        |> Pipeline.required "inserted_ago" JD.string


encodeMessage : Message -> JE.Value
encodeMessage record =
    JE.object
        [ ( "team_id", JE.int <| record.team_id )
        , ( "sender_id", JE.int <| record.sender_id )
        , ( "channel_id", JE.int <| record.channel_id )
        , ( "thread_id", JE.int <| record.thread_id )
        , ( "body", JE.string <| record.body )
        ]


encodeThread : Thread -> JE.Value
encodeThread v =
    JE.object
        [ ( "subject", JE.string v.subject )
        , ( "channel_id", JE.int v.channel_id )
        , ( "team_id", JE.int v.team_id )
        , ( "user_id", JE.int v.user_id )
        ]


encodeThreadNMessage : ThreadNMessage -> JE.Value
encodeThreadNMessage record =
    JE.object
        [ ( "message", encodeMessage record.message )
        , ( "thread", encodeThread record.thread )
        ]


encodeChannel : { team_id : Int, name : String } -> JE.Value
encodeChannel record =
    JE.object
        [ ( "team_id", JE.int <| record.team_id )
        , ( "name", JE.string <| record.name )
        ]


encodeTeam : { name : String } -> JE.Value
encodeTeam record =
    JE.object
        [ ( "name", JE.string <| record.name )
        ]
