module Api.User exposing (Env(..), Model(..), Ping, Team, Token, TokenData, TokenMeta, UserAuth, decodePing, decodeTeam, decodeToken, decodeTokenData, decodeTokenMeta, encodeTeam, encodeToken, encodeTokenData, encodeTokenMeta, encodeUserAuth, envDecoder, initialModel)

import Json.Decode as JD exposing (field)
import Json.Decode.Pipeline as Pipeline exposing (optional, required)
import Json.Encode as JE exposing (..)
import RemoteData exposing (WebData)
import RemoteData.Http


type Model
    = UserAuthData UserAuth
    | WebDatax Token


type alias UserAuth =
    { email : String
    , password : String
    }


type Env
    = Production
    | Development


type alias Token =
    { meta : TokenMeta
    , data : TokenData
    , teams : List Team
    }


type alias TokenMeta =
    { token : String
    }


type alias TokenData =
    { name : String
    , id : Int
    , email : String
    }


type alias Ping =
    { ok : String }


type alias Team =
    { name : String
    , id : Int
    , descr : String
    }


initialModel : Model
initialModel =
    UserAuthData { email = "jonsnow@example.com", password = "secret" }



-- json decoders / encoders


encodeUserAuth : UserAuth -> JE.Value
encodeUserAuth record =
    JE.object
        [ ( "email", JE.string <| record.email )
        , ( "password", JE.string <| record.password )
        ]


decodeToken : JD.Decoder Token
decodeToken =
    JD.map3 Token
        (field "meta" decodeTokenMeta)
        (field "data" decodeTokenData)
        (field "teams" (JD.list decodeTeam))


envDecoder : JD.Decoder Env
envDecoder =
    JD.string
        |> JD.andThen
            (\str ->
                case str of
                    "production" ->
                        JD.succeed Production

                    "development" ->
                        JD.succeed Development

                    somethingElse ->
                        JD.fail <| "Unknown env: " ++ somethingElse
            )


decodePing : JD.Decoder Ping
decodePing =
    JD.map Ping
        (field "ok" JD.string)


decodeTeam : JD.Decoder Team
decodeTeam =
    JD.succeed Team
        |> Pipeline.required "name" JD.string
        |> Pipeline.required "id" JD.int
        |> Pipeline.optional "descr" JD.string ""


decodeTokenMeta : JD.Decoder TokenMeta
decodeTokenMeta =
    JD.map TokenMeta
        (field "token" JD.string)


decodeTokenData : JD.Decoder TokenData
decodeTokenData =
    JD.map3 TokenData
        (field "name" JD.string)
        (field "id" JD.int)
        (field "email" JD.string)


encodeToken : Token -> JE.Value
encodeToken record =
    JE.object
        [ ( "meta", encodeTokenMeta <| record.meta )
        , ( "data", encodeTokenData <| record.data )
        , ( "teams", JE.list encodeTeam record.teams )
        ]


encodeTeam : Team -> JE.Value
encodeTeam record =
    JE.object
        [ ( "name", JE.string <| record.name )
        , ( "id", JE.int <| record.id )
        , ( "descr", JE.string <| record.descr )
        ]


encodeTokenMeta : TokenMeta -> JE.Value
encodeTokenMeta record =
    JE.object
        [ ( "token", JE.string <| record.token )
        ]


encodeTokenData : TokenData -> JE.Value
encodeTokenData record =
    JE.object
        [ ( "name", JE.string <| record.name )
        , ( "id", JE.int <| record.id )
        , ( "email", JE.string <| record.email )
        ]
