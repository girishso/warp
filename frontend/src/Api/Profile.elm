module Api.Profile exposing (Model, initialModel)

import Api.User as User
import Types exposing (..)


type alias Model =
    { email : String, name : String, teams : List Team }


initialModel : Context -> Model
initialModel ctx =
    let
        ( email, name, teams ) =
            case ctx.token of
                Just token ->
                    ( token.data.email, token.data.name, token.teams )

                Nothing ->
                    ( "", "", [] )
    in
    Model email name teams
