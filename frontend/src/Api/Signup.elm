module Api.Signup exposing (Model(..), UserAuth, encodeUserAuth, initialModel)

import Api.User as User
import Json.Encode as JE exposing (..)


type alias UserAuth =
    { name : String
    , email : String
    , password : String
    }


type Model
    = UserAuthData UserAuth
    | WebDatax User.Token


initialModel : Model
initialModel =
    UserAuthData { name = "Jon Snow", email = "jonsnow@example.com", password = "secret" }


encodeUserAuth : UserAuth -> JE.Value
encodeUserAuth record =
    JE.object
        [ ( "name", JE.string <| record.name )
        , ( "email", JE.string <| record.email )
        , ( "password", JE.string <| record.password )
        ]
