<!-- 9th Aug 2017
  - TODO
    - x select first thread by default (and render it's messages)
    - x after selecting a channel, select first thread by default
    - x do Elm Dicts maintain order? -- they don't :(

17th Aug 2017
  - TODO
    - api
      - x need to send inserted_at timestamps
      - x check sql queries `order by`, should be most recent first
      - add indexes in migrations for `inserted_at`, `updated_at` for all tables
    - fe
      - x display teams
      - x display threads for the team (inbox)
      - x display channels
      - x display threads for the channel
  - Done
    - Went back to list of threads, messages etc.
    - select first thread by default (and render it's messages) -->

9th Nov 2017
  - TODO
    - api
      - add indexes in migrations for `inserted_at`, `updated_at` for all tables
      - time ago timestamps, along with full times (use timex)
      - phoenix channel for notifications
      - pagination
      - invite team members
      - search
      - list all users for team not messages
      - channel has many subscribers (users)
      - crash if no channels present in team.. while creating new thread
    - fe
      - style
      - channel selectable/random tag colors
      - invite team members
      - when going to login page see if Already logged in
      - onboarding flow
      - post new msg.. reuse from post new thread
      - New team
